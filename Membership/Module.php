<?php

namespace Modules\Membership;

use Modules\Framework\Core\Config\Config;

class Module extends \Modules\Framework\Core\ModuleSystem\Module {
	public $dependencies = array("Framework");
	public $autoInstall = true;

	public function install() {
		\Modules\Framework\Core\DB::Command()->query(
			sprintf(
				"CREATE TABLE IF NOT EXISTS `%sUser` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`login` varchar(255) NOT NULL,
					`fname` varchar(50) NOT NULL,
					`mname` varchar(50) NOT NULL,
					`lname` varchar(50) NOT NULL,
					`password` varchar(255) NOT NULL,
					`isSystem` int(1) NOT NULL DEFAULT '0',
					PRIMARY KEY (`id`),
					UNIQUE KEY `login` (`login`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1", 
					Config::getInstance()->connection['dbPrefix']));

		$db = \Modules\Framework\Core\DB::Command()->prepare(
			sprintf(
				"INSERT INTO `%sUser` (login, password, isSystem)
				 VALUES (:login, :password, :isSystem)
				 ON DUPLICATE KEY UPDATE password = VALUES(password)", 
					Config::getInstance()->connection['dbPrefix']));

		$db->execute(array(
			'login' => 'admin',
			'password' => md5(\Modules\Framework\Core\Config\Config::getInstance()->passwordSalt . "123456"),
			'isSystem' => true
		));
		
		parent::install();
	}
}