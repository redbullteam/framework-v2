<?php
/**
 * @var \Modules\Membership\Models\User $model
 * @var \Modules\Framework\Core\Metadata\DynamicMetadata $fields
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 */
$this->extend('~:Layouts/general');
?>
<?php $__section->begin('body'); ?>
	<form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>
				<?=$pageTitle?>
			</legend>

			<?php foreach ($fields as $fieldName => $field): ?>
				<?php if ($fieldName == "isSystem") continue; ?>
				<?php if ($field->validation->datatype == \Modules\Framework\Validation\Validator\DataType::HIDDEN): ?>
					<input type="hidden" id="<?=$fieldName?>" name="<?=$fieldName?>" value="<?=$model->$fieldName?>">
					<?php continue; ?>
				<?php endif; ?>
				<div class="control-group <?php if ($field->validation->errorMessage): ?>error<?php endif; ?>">
					<?php if ($field->type != "checkbox" && $field->type != "radio"):?>
						<label class="control-label" for="<?=$fieldName?>"><?=$field->display?><?php if ($field->validation->required): ?>*<?php endif; ?></label>
					<?php endif; ?>

					<div class="controls">
						<?php if ($field->type == "checkbox" || $field->type == "radio"): ?>
							<label class="checkbox">
						<?php endif; ?>

						<?php switch ($field->validation->datatype):
							case "select": ?>
								<select class="input-xlarge" id="<?=$fieldName?>" name="<?=$fieldName?>" <?php if ($field->validation->required): ?>required<?php endif; ?>>
									<option></option>
									<?php $options = call_user_func(array($field->model, 'LoadCollection'), null, "id, title", "title"); ?>
									<?php foreach ($options as $option): ?>
										<option value="<?=$option->id?>" <?php if ($model->$fieldName == $option->id): ?>selected<?phpendif?>><?=$option->title?></option>
									<?php endforeach; ?>
								</select>
								<?php break; ?>
							<?php case \Modules\Framework\Validation\Validator\DataType::MULTILINETEXT: ?>
								<textarea class="input-xlarge" id="<?=$fieldName?>" name="<?=$fieldName?>" <?php if ($field->required == "true"): ?>required<?php endif; ?>><?=htmlspecialchars($model->$fieldName ?: $_REQUEST[$fieldName])?></textarea>
								<?php break; ?>
							<?php case \Modules\Framework\Validation\Validator\DataType::BOOLEAN: ?>
								<input type="<?=$field->type?>" class="input-xlarge" id="<?=$fieldName?>" name="<?=$fieldName?>" value="1" <?php if ($model->$fieldName): ?>checked<?php endif; ?>>
								<?php break; ?>
							<?php default: ?>
								<input type="<?=$field->type?>" class="input-xlarge" id="<?=$fieldName?>" name="<?=$fieldName?>" value="<?=htmlspecialchars($model->$fieldName ?: $_REQUEST[$fieldName])?>" <?php if ($field->type == "date"): ?>data-date-format="yyyy-mm-dd" data-date-language="ru"<?php endif; ?> <?php if ($field->required == "true"): ?>required<?php endif; ?>>
								<?php break; ?>
						<?php endswitch; ?>

						<?php if ($field->type == "checkbox" || $field->type == "radio"): ?>
								<?=$field->label?>
							</label>
						<?php endif; ?>

						<?php if ($field->validation->errorMessage): ?>
							<span class="help-inline"><?=$field->validation->errorMessage?></span>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>

			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Применить</button>
				<a href="<?=$backwardUrl?>" class="btn"><i class="icon-remove"></i> Отмена</a>
			</div>
		</fieldset>
	</form>
<?php $__section->end(); ?>