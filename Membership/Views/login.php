<?php
use \Modules\Framework\Globalization\i18n as i18n;
/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Membership\Models\User $model
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var int $page
 */
$this->extend('~:Layouts/general');
?>
<?php $__section->begin('body'); ?>
	<form action="" class="form-horizontal" method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>
				<?=i18n::current()->signInToSite;?>
			</legend>

			<?php if (array_key_exists("unknown", $validation)): ?>
				<div class="alert alert-error">
					<?=sprintf(i18n::current()->invalidPassword, $model->login);?>
				</div>
			<?php endif; ?>

			<div class="control-group <?php if (array_key_exists("login", $validation)): ?>error<?php endif; ?>">
				<label class="control-label" for="alias"><?=i18n::current()->login;?>*</label>
				<div class="controls">
					<input type="text" class="input-xlarge" id="login" name="login" value="<?=htmlspecialchars($model->login)?>" required>
					<?php if (array_key_exists("login", $validation)): ?>
						<span class="help-inline"><?=i18n::current()->required;?></span>
					<?php endif; ?>
				</div>
			</div>

			<div class="control-group <?php if (array_key_exists("password", $validation)): ?>error<?php endif; ?>">
				<label class="control-label" for="password"><?=i18n::current()->password;?>*</label>
				<div class="controls">
					<input type="password" class="input-xlarge" id="password" name="password" value="<?=htmlspecialchars($model->password)?>" required>
					<?php if (array_key_exists("password", $validation)): ?>
						<span class="help-inline"><?=i18n::current()->required;?></span>
					<?php endif; ?>
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> <?=i18n::current()->signIn;?></button>
				<a href="/registration" class="btn btn-success"><i class="icon-user icon-white"></i> <?=i18n::current()->register;?></a>
			</div>
		</fieldset>
	</form>
<?php $__section->end(); ?>