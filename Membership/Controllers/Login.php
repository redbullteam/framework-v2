<?php

namespace Modules\Membership\Controllers;

use Modules\Membership\Models\User;

/**
 * Вход на сайт
 */
class Login extends \Modules\Framework\Mvc\Controller\Controller {
	public function get($redirectUrl = "/profile", $validation = array()) {
		if (User::getInstance()->id) {
			self::redirect($redirectUrl);
		}

		self::view("login",
			array(
				"pageTitle" => "Вход на сайт",
				"redirectUrl" => $redirectUrl,
				"validation" => $validation,
				"model" => User::getInstance()
			));
	}

	public function post($redirectUrl = "/profile") {
		$user = User::getInstance();
		
		$user->map($_POST, $_FILES);
		
		$user->Login();
		
		if ($user->id) {
			self::redirect($redirectUrl);
		} else {
			$validation = array();
			
			if (!$_POST["login"]) {
				$validation[] = "login";
			}
			
			if (!$_POST["password"]) {
				$validation[] = "password";
			}
			
			if ($_POST["alias"] && $_POST["password"]) {
				$validation[] = "unknown";
			}
			
			$this->get($redirectUrl, $validation);
		}
	}
}

?>