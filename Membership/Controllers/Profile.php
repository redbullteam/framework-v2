<?php

namespace Modules\Membership\Controllers;

use Modules\Membership\Models\User;

/**
 * Профиль пользователя
 */
class Profile extends \Modules\Framework\Mvc\Controller\Controller {
	public function get($validation = array()) {
		self::view("profile",
			array(
				"pageTitle" => "Профиль пользователя",
				"model" => User::getInstance(),
				"fields" => \Modules\Framework\Core\Metadata\MetadataProvider::getInstance()->getPropertyMetadata('\Modules\Membership\Models\User'),
				"validation" => $validation
			));
	}

	public function post() {
		$model = User::getInstance();
		$model->Map($_POST, $_FILES);

		try {
			$model->Validate();

			$model->Save();

			self::redirect("/profile");
		}
		catch (\Modules\Framework\Validation\ValidateException $e) {
			$this->get($e->fields);
		}
	}
	
	public function onActionExecuting() {
		if (!User::getInstance()->id) {
			self::redirect(sprintf("/login?redirectUrl=%s", urlencode(\Modules\Framework\Core\Application::getInstance()->getUrlCurrent())));
		}
	}
}

?>