<?php

namespace Modules\Membership\Controllers;

use Modules\Membership\Models\User;

/**
 * Регистрация
 */
class Registration extends \Modules\Framework\Mvc\Controller\Controller {
	public function get($metadata = null) {
		self::view("profile",
			array(
				"pageTitle" => "Регистрация",
				"model" => User::getInstance(),
				"fields" => $metadata ?: \Modules\Framework\Core\Metadata\MetadataProvider::getInstance()->getPropertyMetadata('\Modules\Membership\Models\User')
			));
	}

	public function post($redirectUrl = "/profile") {
		$model = User::getInstance();
		$model->map($_POST, $_FILES);

		try {
			$model->validate();

			$model->save();
			
			$model->login();

			self::redirect($redirectUrl);
		}
		catch (\Modules\Framework\Validation\ValidateException $e) {
			$this->get($redirectUrl, $e->fields);
		}
	}

	public function onActionExecuting($redirectUrl = "/profile") {
		if (User::getInstance()->id) {
			self::redirect($redirectUrl);
		}
	}
}

?>