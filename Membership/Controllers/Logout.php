<?php

namespace Modules\Membership\Controllers;

use Modules\Membership\Models\User;

/**
 * Выход c сайт
 */
class Logout extends \Modules\Framework\Mvc\Controller\Controller {
	public function get($redirectUrl = "/") {
		User::getInstance()->logout();
		
		if (User::getInstance()->id) {
			self::redirect($redirectUrl);
		}
	}

	public function post($redirectUrl = "/") {
		User::getInstance()->logout();
		
		if (User::getInstance()->id) {
			self::redirect($redirectUrl);
		}
	}
}

?>