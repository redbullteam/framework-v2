<?php

namespace Modules\Membership\Models;

use Modules\Framework\Core\Config\Config;

/**
 * @Display("Пользователи")
 */
class User extends \Modules\Framework\Mvc\Model\BaseModel {
	/**
	 * @var User
	 */
	private static $instance = null;

	/**
	 * @Display("Фамилия")
	 * @Required
	 */
	public $lname;

	/**
	 * @Display("Имя")
	 * @Required
	 */
	public $fname;

	/**
	 * @Display("Отчество")
	 */
	public $mname;

	/**
	 * @Display("Логин")
	 * @Required
	 */
	public $login;

	/**
	 * @Display("Пароль")
	 * @DataType(DataType::PASSWORD)
	 * @Required
	 * @HideFromList
	 */
	public $password;

	/**
	 * @Display("Администратор сайта")
	 * @DataType(DataType::BOOLEAN)
	 * @Debug
	 */
	public $isSystem;

	/**
	 * @param string $login
	 * @param string $password
	 */
	public function __construct($login = '', $password = '') {
		$this->login = $login;
		$this->password = $password;

		$this->isSystem = 0;
	}

	protected function beforeSave() {
		$this->password = md5(Config::getInstance()->passwordSalt . $this->password);
	}

	/**
	 * @static
	 * @return User
	 */
	public static function getInstance() {
		if (self::$instance == null) {
			self::$instance = new User(isset($_COOKIE["login"]) ? $_COOKIE["login"] : "", isset($_COOKIE["password"]) ? $_COOKIE["password"] : "");

			self::$instance->login(false);
		}

		return self::$instance;
	}

	public function login($doMd5 = true) {
		if ($this->login && $this->password) {
			self::$instance->Load(array("login" => $this->login, "password" => $doMd5 ? md5(Config::getInstance()->passwordSalt . $this->password) : $this->password));

			if ($this->id) {
				setcookie("login", $this->login, time() + 864000, "/");
				setcookie("password", $this->password, time() + 864000, "/");
			}
		}
	}

	public function logout() {
		setcookie("login", null, -1, "/");
		setcookie("password", null, -1, "/");
	}
}