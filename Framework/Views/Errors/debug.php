<?php
/**
 * @var \Exception $e
 */
?><html>
<head>
    <meta charset="utf-8">
    <title><?=\Modules\Framework\Globalization\i18n::current()->error?></title>
    <link rel="stylesheet" href="/Resources/Admin/css/bootstrap.min.css">
</head>
<body>
	<h1><?=\Modules\Framework\Globalization\i18n::current()->exception?>: <?=$e->getMessage()?></h1>
	<h2><?=\Modules\Framework\Globalization\i18n::current()->file?>: <?=$e->getFile()?></h2>
	<h2><?=\Modules\Framework\Globalization\i18n::current()->line?>: <?= $e->getLine()?></h2>
	<h3><?=\Modules\Framework\Globalization\i18n::current()->trace?>:</h3>
	<pre><?php print_r($e->getTraceAsString())?></pre>
	<h3><?=\Modules\Framework\Globalization\i18n::current()->details?>:</h3>
	<pre><?php print_r($e)?></pre>
	<h3><?=\Modules\Framework\Globalization\i18n::current()->dump?>:</h3>
	<pre><?php debug_print_backtrace()?></pre>
</body>
</html>