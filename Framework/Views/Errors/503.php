<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=\Modules\Framework\Globalization\i18n::current()->error?></title>
    <link rel="stylesheet" href="/Resources/Admin/css/bootstrap.min.css">
</head>
<body>
<h1><?=\Modules\Framework\Globalization\i18n::current()->websiteTemporarilyUnavailable?></h1>
</body>
</html>