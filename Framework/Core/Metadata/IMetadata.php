<?php

namespace Modules\Framework\Core\Metadata;

interface IMetadata {
	function getMetadataKey();
}
