<?php

namespace Modules\Framework\Core\Metadata;

class FullMetadata {
	public $class;

	public $fields;

	public function __construct() {
		$this->class = new DynamicMetadata();
		$this->fields = new DynamicMetadata();
	}
}
