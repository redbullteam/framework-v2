<?php

namespace Modules\Framework\Core\Metadata;

use Modules\Framework\Core\Application;
use Modules\Framework\Core\Metadata\FullMetadata;
use Modules\Framework\Cache\Cache;
use Modules\Framework\Core\ModuleSystem\ModuleManager;

class MetadataProvider implements IMetadataProvider {
	private static $CACHE_SALT = '@[Meta]';

	/**
	 * @var MetadataProvider
	 */
	private static $instance = null;

	private $loadedMetadata = array();

	/*
	 * @var \Doctrine\Common\Annotations\CachedReader
	 */
	private $annotationReader;

	public function __construct() {
		$this->annotationReader = !Application::getInstance()->isDebugMode()
			? new \Doctrine\Common\Annotations\CachedReader(
					ModuleManager::getInstance()->annotationReader,
					Cache::getInstance()->getCacheProvider(),
					false
				)
			: ModuleManager::getInstance()->annotationReader;
	}

	/*
	 * @return \Modules\Framework\Core\Metadata\FullMetadata
	 */
	public function getFullMetadata($className) {
		if (isset($this->loadedMetadata[$className])) {
			return $this->loadedMetadata[$className];
		}

		$metadata = !Application::getInstance()->isDebugMode()
			? Cache::getInstance()->fetch($className . self::$CACHE_SALT)
			: false;

		if ($metadata !== false)
			return $metadata;

		$metadata = new FullMetadata();

		$reflectionClass = new \ReflectionClass($className);

		$classAnnotations = $this->annotationReader->getClassAnnotations($reflectionClass);
		$metadata->class = $this->getMemberMetadata($classAnnotations);

		$reflectionProperties = $reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC);

		foreach ($reflectionProperties as $reflectionProperty) {
			$propertyAnnotations = $this->annotationReader->getPropertyAnnotations($reflectionProperty);

			$ignore = array_filter($propertyAnnotations, function ($item) {
				return ($item instanceof \Modules\Framework\Annotations\Ignore);
			});

			if (empty($ignore)) {
				$fieldName = $reflectionProperty->getName();
				$metadata->fields[$fieldName] = $this->getMemberMetadata($propertyAnnotations);
				$metadata->fields[$fieldName]->fieldName = $fieldName;
			}
		}

		if (!Application::getInstance()->isDebugMode()) {
			Cache::getInstance()->save($className . self::$CACHE_SALT, $metadata);
		}

		$this->loadedMetadata[$className] = $metadata;

		return $metadata;
	}

	/*
	 * @return \Modules\Framework\Core\Metadata\DynamicMetadata
	 */
	public function getClassMetadata($className) {
		$metadata = $this->getFullMetadata($className);

		return $metadata->class;
	}
	/*
	 * @return \Modules\Framework\Core\Metadata\DynamicMetadata
	 */
	public function getPropertyMetadata($className, $propertyName = null) {
		$metadata = $this->getFullMetadata($className);

		return $propertyName ? $metadata->fields[$propertyName] : $metadata->fields;
	}

	private function getMemberMetadata(array $annotations) {
		$metadata = new DynamicMetadata();

		if (!empty($annotations)) {
			foreach ($annotations as $annotation) {
				if (!($annotation instanceof IMetadata))
					continue;

				$namespace = strtok($annotation->getMetadataKey(), '.');
				$name = strtok('.');

				if ($name) {
					$metadata->$namespace = $metadata->$namespace ?: new DynamicMetadata();
					$metadata->$namespace->$name = $annotation;
				} else {
					$metadata->$namespace = $annotation;
				}
			}
		}

		return $metadata;
	}

	/**
	 * @static
	 * @return MetadataProvider
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new MetadataProvider();
		}

		return self::$instance;
	}
}
