<?php

namespace Modules\Framework\Core\Metadata;

interface IMetadataProvider {
	/*
	 * @return \Modules\Framework\Core\Metadata\FullMetadata
	 */
	function getFullMetadata($className);
	/*
	 * @return \Modules\Framework\Core\Metadata\DynamicMetadata
	 */
	function getClassMetadata($className);
	/*
	 * @return \Modules\Framework\Core\Metadata\DynamicMetadata
	 */
	function getPropertyMetadata($className, $propertyName = null);
}
