<?php

namespace Modules\Framework\Core;

class Application {
	/**
	 * @var Application
	 */
	private static $instance = null;

	/**
	 * @var boolean
	 */
	private $debug;

	/**
	 * @var string
	 */
	private $dataDirectory;

	/**
	 * @var string
	 */
	private $frameworkBaseDirectory;

	/**
	 * @var array
	 */
	private $autoLoads;

	/**
	 * @var \Modules\Framework\Core\UniversalClassLoader
	 */
	private $loader;

	public function __construct() {
		$this->debug = false;
	}

	/**
	 * Запустить приложение сайта
	 */
	public function run($forceInstall = null) { 
		$this->frameworkBaseDirectory = dirname(dirname(__DIR__));
		$this->dataDirectory = $this->frameworkBaseDirectory . '/../../Data/';
		$applicationDirectory = $this->frameworkBaseDirectory . '/../../Application/';

		try {
			$this->getLoader();
			$psr4Paths = $this->loader->getPrefixesPsr4();

			$this->registerErrorHandler();

			ModuleSystem\ModuleManager::getInstance()
				->compose($psr4Paths['Modules\\'], $forceInstall ?: array(), $this->debug);

			\Modules\Framework\Mvc\Routing\Router::getInstance()
				->dispatch();

		} catch (\Exception $e) {
			\Modules\Framework\Utils\Logger::Add($e);

			if ($this->isDebugMode()) {
				echo new \Modules\Framework\Mvc\View\PhpView("Errors/debug", array("e" => $e), $this->frameworkBaseDirectory . '/Framework/Views/');
			} else {
				if ($e instanceof \Modules\Framework\Mvc\Routing\HttpException) {
					header('HTTP/1.1 404 Page not found');
					echo new \Modules\Framework\Mvc\View\PhpView("Errors/404", null, $this->frameworkBaseDirectory . '/Framework/Views/');
				} else {
					header('HTTP/1.1 503 Service Unavailable');
					echo new \Modules\Framework\Mvc\View\PhpView("Errors/503", null, $this->frameworkBaseDirectory . '/Framework/Views/');
				}
			}
		}
	}

    /**
	 * Статический экземпляр класса
	 *
     * @static
     * @return Application
     */
    public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new Application();
		}

		return self::$instance;
	}

	/**
	 * @return \Modules\Framework\Core\UniversalClassLoader
	 */
	public function getLoader() {
		if ($this->loader == null) {
			$this->loader = require_once $this->getFrameworkBaseDirectory() . '/../autoload.php';

			//$this->loader->registerNamespaces($this->getAutoLoads());
		}

		return $this->loader;
	}

	private function registerErrorHandler() {
		$typeHinting =  new ErrorHandling\ErrorHandler();
		$typeHinting->register();
	}

    /**
     * @param $debug
     * @return Application
     */
    public function setDebug($debug) {
        $this->debug = $debug;

        return $this;
    }

	public function isDebugMode() {
		return $this->debug === true;
	}

	/*
	 * @return string
	 */
	public function mode() {
		if (getenv("SITE_MODE") == "Development")
			return "development";

		return "production";
	}

	public function getFrameworkBaseDirectory() {
		return $this->frameworkBaseDirectory;
	}

	public function getDataDirectory() {
		return $this->dataDirectory;
	}

	/**
	 * @param $value string new data directory
	 * @return Application
	 */
	public function setDataDirectory($value) {
		$this->dataDirectory = rtrim($value, \DIRECTORY_SEPARATOR) . \DIRECTORY_SEPARATOR;
		return $this;
	}
}