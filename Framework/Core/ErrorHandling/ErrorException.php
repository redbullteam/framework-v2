<?php

namespace Modules\Framework\Core\ErrorHandling;

class ErrorException extends \Exception {
	protected $_context = null;

	public function getContext() {
		return $this->_context;
	}

	public function setContext($value) {
		$this->_context = $value;
	}

	public function __construct($code, $message, $file, $line, $context) {
		parent::__construct($message, $code);

		$this->file = $file;
		$this->line = $line;
		$this->setContext($context);
	}
}
