<?php

namespace Modules\Framework\Core\ErrorHandling;

class ErrorHandler {
	function phpErrorHandler($code, $message, $file, $line, $context) {
		if (\E_ERROR & $code | \E_WARNING & $code) {
			if ($code == E_RECOVERABLE_ERROR) { // Scalar Type-Hinting patch.
				$regexp = '/^Argument (\d)+ passed to (.+) must be an instance of (?<hint>.+), (?<given>.+) given/i';

				if (preg_match($regexp, $message, $match)) {
					$given = $match[ 'given' ] ;
					$hint  = end(explode('\\', $match['hint'])) ; // namespaces support.

					if ($hint == $given)
						return true;

					throw new \Exception($message);
				}
			}

			throw new \Modules\Framework\Core\ErrorHandling\ErrorException($code, $message, $file, $line, $context);
		}

		if (\Modules\Framework\Core\Application::getInstance()->isDebugMode() && (\E_NOTICE & $code)) {
			\Modules\Framework\Utils\Logger::Add(new \Modules\Framework\Core\ErrorHandling\ErrorException($code, $message, $file, $line, $context));
		}
	}

	function register() {
		set_error_handler(array($this, 'phpErrorHandler'));
	}
}
