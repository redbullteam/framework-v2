<?php

namespace Modules\Framework\Core\ModuleSystem;

class Module {
	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $namespace;

	/**
	 * @var boolean
	 */
	public $autoInstall = true;

	/**
	 * @var boolean
	 */
	public $installed = false;

	/**
	 * @var array
	 */
	public $dependencies = array();

	/**
	 * @var string
	 */
	public $filename = __FILE__;

	public function init() {
	}

	public function install() {
		$this->installed = true;
	}

	public function remove() {
		$this->installed = false;
	}
}