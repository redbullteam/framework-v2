<?php

namespace Modules\Framework\Core\ModuleSystem;

use Modules\Framework\Cache\ClassDataCache;
use Doctrine\Common\Annotations\SimpleAnnotationReader;

class ModuleManager {
	/**
	 * @var ModuleManager
	 */
	private static $instance = null;

	/**
	 * @var Module[]
	 */
	private $modules;

	/**
	 * @var SimpleAnnotationReader
	 */
	public $annotationReader;


	private function __construct() {
		$this->annotationReader = new SimpleAnnotationReader();
	}

	/**
	 * Подключить модули
	 * @param mixed $modulesDirs Массив директорий с модулями
	 * @param mixed $forceInstall Массив названий модулей, которые нужно установить независимо от значения autoInstall
	 * @param bool $recompose Загружать ли модули из кэша
	 * @throws DependencyResolvingException
	 */
	public function compose($modulesDirs, $forceInstall, $recompose = false) {
		if (!$recompose) {
			$this->modules = ClassDataCache::getInstance()->fetch(__CLASS__);

			if ($this->modules !== false) {
				foreach ($this->modules as $module) {
                    if (!$module->autoInstall)
                        continue;

					$module->init();
				}

				return;
			}
		}

		$directoryIterator = new \AppendIterator();

		if (!is_array($modulesDirs)) {
			if (is_dir($modulesDirs))
				$directoryIterator->append(new \RecursiveDirectoryIterator($modulesDirs,
					\FilesystemIterator::CURRENT_AS_PATHNAME | \FilesystemIterator::SKIP_DOTS));
		} else {
			foreach ($modulesDirs as $modulesDir) {
				if (is_dir($modulesDir))
					$directoryIterator->append(new \RecursiveDirectoryIterator($modulesDir,
						\FilesystemIterator::CURRENT_AS_PATHNAME | \FilesystemIterator::SKIP_DOTS));
			}
		}

		$resolved = array();
		$resolvedNames = array();
		$unresolved = array();
		
		if (!is_array($forceInstall))
			$forceInstall = array($forceInstall);

		foreach ($directoryIterator as $path) {
			$moduleFileName = $path . \DIRECTORY_SEPARATOR . 'Module.php';

			if (!file_exists($moduleFileName))
				continue;

			$name = end(explode(\DIRECTORY_SEPARATOR, $path));
			$namespace = 'Modules\\' . $name;

			$className = "$namespace\\Module";

			/* @var $module \Modules\Framework\Core\ModuleSystem\Module */
			$module = new $className();
			$module->filename = $moduleFileName;
			$module->namespace = $namespace;
			if (!$module->name)
				$module->name = $name;
				
			if (in_array($module->name, $forceInstall))
				$module->autoInstall = true;

			if (count(array_diff($module->dependencies, $resolvedNames))== 0) {
				$resolved[] = $module;
				$resolvedNames[] = $module->name;
			} else {
				$unresolved[] = $module;
			}
		}

		while(!empty($unresolved)) {
			$changed = false;

			foreach ($unresolved as $idx => $module) {
				if (count(array_diff($module->dependencies, $resolvedNames))== 0) {
					$resolved[] = $module;
					$resolvedNames[] = $module->name;
					unset($unresolved[$idx]);
					$changed = true;
					break;
				}
			}

			if (!$changed)
				throw new DependencyResolvingException(sprintf("Unable to resolve dependencies for modules: %s",
					implode(', ', array_map(function ($module) { return $module->name; }, $unresolved))));
		}

		$this->modules = $resolved;

		foreach ($this->modules as $module) {
			if (!$module->autoInstall)
				continue;
		
			$module->init();
		}

		foreach ($this->modules as $module) {
			if (!$module->autoInstall)
				continue;
				
			$module->install();
		}

		ClassDataCache::getInstance()->save(__CLASS__, $this->modules);
	}

	/**
	 * @return Module[]
	 */
	public function getAvailableModules() {
		return $this->modules;
	}

	/**
	 * @return Module[]
	 */
	public function getInstalledModules() {
		/* @var $module \Modules\Framework\Core\ModuleSystem\Module */
		return array_filter($this->modules, function ($module) {
				return $module->installed;
			});
	}
	
	/**
	 * @return bool
	 */
	public function isModuleInstalled($moduleName) {
		/* @var $module \Modules\Framework\Core\ModuleSystem\Module */
		foreach ($this->modules as $module) {
			if ($module->installed)
				return true;
		}
		
		return false;
	}

	public function registerAnnotationsNamespace($namespace) {
		$this->annotationReader->addNamespace($namespace);
	}

	/**
	 * @static
	 * @return ModuleManager
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new ModuleManager();
		}

		return self::$instance;
	}
}