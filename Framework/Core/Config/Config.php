<?php

namespace Modules\Framework\Core\Config;

use Modules\Framework\Cache\ClassDataCache;

class Config extends \Modules\Framework\Utils\DynamicObject implements IConfig {
	/**
	 * @var Config
	 */
	private static $instance = null;

	/*
	 * @param string $directory
	 */
	public function load($directory) {
		$filename = $directory . \DIRECTORY_SEPARATOR . \Modules\Framework\Core\Application::getInstance()->mode() . ".php";

		if (!is_file($filename)) {
			$filename = $directory . \DIRECTORY_SEPARATOR . "production.php";

			if (!is_file($filename))
				return false;
		}

		$config = include $filename;

		$this->add($config);

		return true;
	}

	public function save() {
		ClassDataCache::getInstance()->save(__CLASS__, $this);

		return $this;
	}

	/**
	 * @static
	 * @return Config
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = ClassDataCache::getInstance()->fetch(__CLASS__);

			if (self::$instance === false)
				self::$instance = new Config();
		}

		return self::$instance;
	}
}
