<?php

namespace Modules\Framework\Core\Config;

interface IConfig {
	function add($key, $value = null);
	function load($directory);
	function save();
}
