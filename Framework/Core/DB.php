<?php

namespace Modules\Framework\Core;

use \Modules\Framework\Core\Config\Config;

/**
 * Класс для инициализирования провайдера БД
 */
class DB {
	/**
	 * @var DB
	 */
	private static $instance = NULL;

	/**
	 * @var \PDO
	 */
	protected $pdo;

	/**
	 * @param string $connectionString
	 * @param string $login
	 * @param string $password
	 */
	function __construct($connectionString, $login, $password) {
		$this->pdo = new \PDO($connectionString, $login, $password, array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

		$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
		$this->pdo->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array('\Modules\Framework\Core\DBStatement', array($this)));
	}

	/**
	 * Статический экземпляр класса
	 *
	 * @static
	 * @return DB
	 */
	private static function getInstance() {
		if(self::$instance == null) {
			$connection = Config::getInstance()->connection;
			self::$instance = new DB($connection['connectionString'], $connection['login'], $connection['password']);
		}

		return self::$instance;
	}

	/**
	 * Доступ к базе по-умолчанию
	 *
	 * @static
	 * @return \PDO
	 */
	public static function Command() {
		return self::getInstance()->pdo;
	}
}

class DBStatement extends \PDOStatement {
	protected $statement;

	protected function __construct($statement) {
		$this->statement = $statement;
	}

	public function execute($data = array()) {
		foreach ($data as $param => $value) {
			if ($param[0] != ':')
				$param = ':' . $param;

			if (is_int($value)) {
				parent::bindParam($param, $value, \PDO::PARAM_INT);
			} else {
				if (is_bool($value)) {
					$value = $value ? 1 : 0;
					parent::bindParam($param, $value, \PDO::PARAM_INT);
				} else {
					if (is_string($value)) {
						parent::bindParam($param, $value, \PDO::PARAM_STR);
					} else {
						if (is_null($value)) {
							parent::bindParam($param, $value, \PDO::PARAM_NULL);
						} else {
							parent::bindParam($param, $value);
						}
					}
				}
			}
		}

		parent::execute($data);

		return $this;
	}

	public function setFetchMode($mode, $className) {
		parent::setFetchMode($mode, $className);

		return $this;
	}
}