<?php

namespace Modules\Framework;

use Modules\Framework\Core\Application;
use Modules\Framework\Core\Config\Config;
use Modules\Framework\Core\ModuleSystem\ModuleManager;
use Doctrine\Common\Annotations\AnnotationRegistry;

class Module extends \Modules\Framework\Core\ModuleSystem\Module {
	private $availableModels = array();
	private $availableControllers = array();

	public function init() {
		AnnotationRegistry::registerLoader(array(Application::getInstance()->getLoader(), "loadClass"));

		ModuleManager::getInstance()->registerAnnotationsNamespace('Modules\\Framework\\Validation\\Validator');
		ModuleManager::getInstance()->registerAnnotationsNamespace('Modules\\Framework\\Annotations');
	}

	public function install() {
		$availableModules = ModuleManager::getInstance()->getAvailableModules();

		foreach ($availableModules as $module) {
			if (!$module->autoInstall)
				continue;
		
			$this->processConfigurations($module);
			$this->processModels($module);
			$this->processControllers($module);
			$this->processViews($module);
			$this->processResources($module);
		}

		Config::getInstance()
			->add('mvc', array(
							'availableModels' => $this->availableModels,
							'availableControllers' => $this->availableControllers
						))
			->save();

		parent::install();
	}

	/**
	 * @param \Modules\Framework\Core\ModuleSystem\Module $module
	 */
	private function processConfigurations($module) {
		$configFilename = dirname($module->filename) . \DIRECTORY_SEPARATOR . 'Configuration';

		Config::getInstance()->load($configFilename);
	}

	/**
	 * @param \Modules\Framework\Core\ModuleSystem\Module $module
	 */
	private function processModels($module) {
		$directory = dirname($module->filename) . \DIRECTORY_SEPARATOR . 'Models';

		if (!is_dir($directory))
			return;

		$directoryIterator = new \RecursiveDirectoryIterator($directory);

		/* @var $fileInfo \SplFileObject */
		foreach ($directoryIterator as $fileInfo) {
			if (!$fileInfo->isFile())
				continue;

			$filename = $fileInfo->getFilename();

			$className = reset(explode('.', $filename));

			$this->availableModels[$className] = $module->namespace . '\\Models\\' . $className;
		}
	}

	/**
	 * @param \Modules\Framework\Core\ModuleSystem\Module $module
	 */
	private function processControllers($module) {
		$directory = dirname($module->filename) . \DIRECTORY_SEPARATOR . 'Controllers';

		if (!is_dir($directory))
			return;

		$directoryIterator = new \RecursiveDirectoryIterator($directory);

		/* @var $fileInfo \SplFileObject */
		foreach ($directoryIterator as $fileInfo) {
			if (!$fileInfo->isFile())
				continue;

			$filename = $fileInfo->getFilename();

			$className = reset(explode('.', $filename));

			$this->availableControllers[$className] = $module->namespace . '\\Controllers\\' . $className;
		}
	}

	private function processViews($module) {
		$targetDirectory = dirname($module->filename) . \DIRECTORY_SEPARATOR . 'Views';

		if (!is_dir($targetDirectory))
			return;

		$link = Application::getInstance()->getDataDirectory() . 'Views';

		if ((is_dir($link) || mkdir($link, 0777, true)) && !is_link($link . \DIRECTORY_SEPARATOR . $module->name))
			symlink($targetDirectory, $link . \DIRECTORY_SEPARATOR . $module->name);
	}

	private function processResources($module) {
		$targetDirectory = dirname($module->filename) . \DIRECTORY_SEPARATOR . 'Resources';

		if (!is_dir($targetDirectory))
			return;

		$link = $_SERVER["DOCUMENT_ROOT"] . \DIRECTORY_SEPARATOR . "Resources";

		if ((is_dir($link) || mkdir($link, 0777, true)) && !is_link($link . \DIRECTORY_SEPARATOR . $module->name))
			symlink($targetDirectory, $link . \DIRECTORY_SEPARATOR . $module->name);
	}
}