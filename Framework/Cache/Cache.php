<?php

namespace Modules\Framework\Cache;

class Cache {
	/**
	 * @var \Doctrine\Common\Cache\Cache
	 */
	private $cacheProvider = null;

	/**
	 * @var ClassDataCache
	 */
	private static $instance = null;

	private function __construct() {
		$cacheProviderName = \Modules\Framework\Core\Config\Config::getInstance()->system['cacheProvider'];

		$this->cacheProvider = new $cacheProviderName(\Modules\Framework\Core\Application::getInstance()->getDataDirectory() . "Cache" . \DIRECTORY_SEPARATOR);
	}

	/**
	 * @static
	 * @return Cache
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * @static
	 * @return \Doctrine\Common\Cache\Cache
	 */
	public function getCacheProvider() {
		return $this->cacheProvider;
	}

	/**
	 * @param string $id
	 * @param callable $callback
	 * @param int $lifeTime
	 * @return bool|mixed
	 */
	public function getOrSet($id, callable $callback, $lifeTime) {
		$result = $this->fetch($id);

		if ($result === false) {
			$result = $callback();

			if (!$this->cacheProvider->save($id, $result, $lifeTime))
				$result = false;
		}

		return $result;
	}

	public function fetch($id) {
		return $this->cacheProvider->fetch($id);
	}

	public function flushAll($tag) {
		return $this->cacheProvider->flushAll($tag);
	}

	public function contains($id) {
		return $this->cacheProvider->contains($id);
	}

	public function save($id, $data, $lifeTime = 0) {
		return $this->cacheProvider->save($id, $data, $lifeTime);
	}

	public function delete($id) {
		return $this->cacheProvider->delete($id);
	}
}
