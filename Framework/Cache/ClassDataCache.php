<?php

namespace Modules\Framework\Cache;

class ClassDataCache extends \Doctrine\Common\Cache\FilesystemCache {
    const EXTENSION = '.cache.data';

	/**
	 * @var ClassDataCache
	 */
	private static $instance = null;

	/**
	 * @param string $id
	 * @return string
	 */
	protected function getFilename($id) {
		return $this->directory . DIRECTORY_SEPARATOR . str_replace('\\', \DIRECTORY_SEPARATOR, $id) . $this->extension;
	}

	/**
	 * @static
	 * @return ClassDataCache
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new self(\Modules\Framework\Core\Application::getInstance()->getDataDirectory() . "Cache" . \DIRECTORY_SEPARATOR);
		}

		return self::$instance;
	}

	/* We use class name as cache id, so we don't need namespaces */

	/**
	 * {@inheritdoc}
	 */
	public function fetch($id) {
		return $this->doFetch($id);
	}

	/**
	 * {@inheritdoc}
	 */
	public function contains($id) {
		return $this->doContains($id);
	}

	/**
	 * {@inheritdoc}
	 */
	public function save($id, $data, $lifeTime = 0) {
		return $this->doSave($id, $data, $lifeTime);
	}

	/**
	 * {@inheritdoc}
	 */
	public function delete($id) {
		return $this->doDelete($id);
	}

	/**
	 * {@inheritdoc}
	 */
	public function flushAll() {
		return call_user_func_array(array($this, "doFlush"), func_get_args());
	}

	/**
	 * {@inheritdoc}
	 */
	protected function doFlush() {
		$tags = func_get_args();
		$tagsPattern = !empty($tags)
			? '(' . implode('|', array_map(function ($tag) {
					return preg_quote(str_replace('\\', \DIRECTORY_SEPARATOR, $tag));
				}, $tags)) . ').*?'
			: '';

		$pattern  = '`^.*?' . $tagsPattern . preg_quote($this->extension) . '$`i';

		$iterator = new \RecursiveDirectoryIterator($this->directory);
		$iterator = new \RecursiveIteratorIterator($iterator);
		$iterator = new \RegexIterator($iterator, $pattern);

		foreach ($iterator as $name => $file) {
			@unlink($name);
		}

		return true;
	}
}