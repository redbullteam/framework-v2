<?php

namespace Modules\Framework\Mvc\Model;

use Modules\Framework\Core\Config\Config;

/**
 * @Display("i18n::tree")
 */
class TreeModel extends BaseModel {
	/**
	 * @Display("i18n::nestedLevel")
	 * @DataType(DataType::HIDDEN)
	 * @HideFromList
	 */
	public $nestedLevel;

	/**
	 * @Display("i18n::sortOrder")
	 */
	public $sortOrder = 3600;

	/**
	 * @var TreeModel
	 */
	protected $parent = null;
	/**
	 * @Display("i18n::parent")
	 * @UIHint("select", sql = "SELECT CONCAT(LPAD('', (LENGTH(nestedLevel) - 1) * 2, '.'), title) as display, id as value FROM %s ORDER BY nestedLevel")
	 * @HideFromList
	 */
	public $parentId;

	public function remove() {
        if (!$this->id)
            return;
        
		$sql = sprintf(
                    "DELETE t1.* FROM `%1\$s%2\$s` t1, `%1\$s%2\$s` t2
                    WHERE t1.nestedLevel LIKE CONCAT(t2.nestedLevel, '%%') AND t2.id = :id", 
						Config::getInstance()->connection['dbPrefix'], 
						$this->getName());
                    
		\Modules\Framework\Core\DB::Command()
            ->prepare($sql)
            ->execute(array('id' => (int) $this->id));
	}

	protected function afterSave() {
		\Modules\Framework\Core\DB::Command()->exec('SET @row := 0;');

		$sql = sprintf($this->parentId > 0 ?
			"UPDATE `%1\$s%2\$s`
			INNER JOIN (SELECT id, @row := @row + 1 AS row FROM `%1\$s%2\$s` WHERE parentId = %3\$d ORDER BY sortOrder) r ON `%1\$s%2\$s`.id = r.id
			JOIN (SELECT id, nestedLevel FROM `%1\$s%2\$s`) p ON `%1\$s%2\$s`.parentid = p.id
			SET `%1\$s%2\$s`.sortOrder = r.row * 10, `%1\$s%2\$s`.nestedLevel = CONCAT(p.nestedLevel, '.', CONV(r.row, 10, 36))
			WHERE parentId = :parentId" :

			"UPDATE `%1\$s%2\$s`
			INNER JOIN (SELECT id, @row := @row + 1 AS row FROM `%1\$s%2\$s` WHERE parentId = %3\$d ORDER BY sortOrder) r ON `%1\$s%2\$s`.id = r.id
			SET `%1\$s%2\$s`.sortOrder = r.row * 10, `%1\$s%2\$s`.nestedLevel = CONV(r.row, 10, 36)
			WHERE parentId = :parentId", 
				Config::getInstance()->connection['dbPrefix'], 
				$this->getName(), 
				(int) $this->parentId);

		\Modules\Framework\Core\DB::Command()
            ->prepare($sql)
            ->execute(array("parentId" => (int) $this->parentId));
	}

	public function getAncestors() {
		$sql = sprintf("SELECT t1.*
				FROM `%1\$s%2\$s` t1, `%1\$s%2\$s` t2
				WHERE t2.nestedLevel LIKE CONCAT(t1.nestedLevel, '.%%') AND t2.id = :id
				ORDER BY t1.nestedLevel", 
					Config::getInstance()->connection['dbPrefix'], 
					$this->getName());

		$result = \Modules\Framework\Core\DB::Command()
			->prepare($sql)
			->execute(array('id' => $this->id))
			->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, get_called_class());

		/* @var BaseModel $model */
		foreach ($result as $model) {
			$model->afterLoad();
		}

		return $result;
	}

	public function getDescendants() {
		$sql = sprintf("SELECT *
				FROM `%1\$s%2\$s`
				WHERE nestedLevel LIKE CONCAT(:nestedLevel, '.%%')
				ORDER BY nestedLevel", 
					Config::getInstance()->connection['dbPrefix'], 
					$this->getName());

		$result = \Modules\Framework\Core\DB::Command()
			->prepare($sql)
			->execute(array('nestedLevel' => $this->nestedLevel))
			->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, get_called_class());

		/* @var BaseModel $model */
		foreach ($result as $model) {
			$model->afterLoad();
		}

		return $result;
	}

	public function getNestedLevel() {
		return count(explode('.', $this->nestedLevel));
	}

	public static function adminLoadCollection($params = null, $columns = null) {
		return self::loadCollection($params, $columns, "nestedLevel");
	}

	public static function group($collection, $groupBy) {
		$result = array();

		if (!is_array($groupBy)) {
			$groupBy = array($groupBy);
		}

		foreach ($groupBy as $fieldName) {
			foreach ($collection as $item)
				$result[$fieldName][$item->$fieldName][] = $item;
		}

		return $result;
	}
}

?>