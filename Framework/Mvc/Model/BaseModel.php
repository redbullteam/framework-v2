<?php

namespace Modules\Framework\Mvc\Model;

use Modules\Framework\Core\Config\Config;
use Modules\Framework\Core\Metadata\MetadataProvider;

/**
 * Базовая модель
 */
class BaseModel {
	/**
	 * @DataType(DataType::HIDDEN)
	 * @HideFromList
	 */
	public $id;

	/**
	 * @Ignore
	 */
	public static $__primaryKey = "id";

	/**
	 * @Ignore
	 */
	public static $__foreignKey = "parentId";

	/**
	 * @Ignore
	 */
	public static $__allowedOperations = array('=', '>', '<', '>=', '<=', '<>', 'like');

	/**
	 * @Ignore
	 */
	protected $__metadata;

	/**
	 * @param int $id
	 */
	public function __construct($id = 0) {
		$this->id = $id;
	}

	/**
	 * Загрузить данные модели из БД
	 *
	 * @static
	 * @param mixed $params [optional]
	 * @return mixed
	 */
	public function load($params = null) {
		if (!$params) {
			$params = array(static::$__primaryKey => $this->{static::$__primaryKey});
		}

		$bindParams = self::modifySqlParams($params);

		$where = !empty($params) ? sprintf('WHERE %s', implode(" AND ", $params)) : "";

		$sql = sprintf('SELECT * FROM `%s%s` %s', 
						Config::getInstance()->connection['dbPrefix'], 
						$this->getName(),
						$where);

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);

		$db->execute($bindParams);

		$db->setFetchMode(\PDO::FETCH_INTO | \PDO::FETCH_PROPS_LATE, $this);

		$result = $db->fetch();

		$this->afterLoad();

		return $result;
	}

	/**
	 * Загрузить список моделей из БД
	 *
	 * @static
	 * @param mixed $params [optional]
	 * @param mixed $columns [optional]
	 * @param string $order [optional]
	 * @param string $limit [optional]
	 * @param int $offset [optional]
	 * @internal param string $model
	 * @return mixed
	 */
	public static function loadCollection($params = null, $columns = null, $order = null, $limit = null, $offset = null) {
		$bindParams = self::modifySqlParams($params);

		$where = !empty($params) ? sprintf('WHERE %s', implode(" AND ", $params)) : "";

		if (empty($columns)) {
			$metadata = MetadataProvider::getInstance()->getPropertyMetadata(get_called_class());

			$columns = array_keys((array)$metadata);
		}

		if (is_array($columns)) {
			$columns = implode(",", array_map(function ($item) { return "`$item`"; }, $columns));
		}

		$sql = sprintf('SELECT %s FROM `%s%s` %s', 
						$columns, 
						Config::getInstance()->connection['dbPrefix'], 
						self::getName(), 
						$where);

		if (!empty($order))
			$sql .= ' ORDER BY ' . $order;

		if (!empty($limit)) {
			if (strpos($limit, ',') !== false) {
				list($offset, $limit) = explode(',', $limit);
			}
			$sql .= ' LIMIT :limit';
			$bindParams[':limit'] = (int) $limit;
		}

		if (!empty($offset)) {
			$sql .= ' OFFSET :offset';
			$bindParams[':offset'] = (int) $offset;
		}

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);

		$db->execute($bindParams);

		$result = $db->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, get_called_class());

		/* @var BaseModel $model */
		foreach ($result as $model) {
			$model->afterLoad();
		}

		return $result;
	}

	public static function modifySqlParams(&$params, $paramPrefix = '') {
		if ($params && is_string($params) && ctype_alnum(str_replace(array('-'), '', $params))) {
			$params = array(static::$__primaryKey => $params);
		}

		$bindParams = array();

		if (is_array($params)) {
			array_walk($params, function(&$item, $key) use (&$bindParams, $paramPrefix) {
				$name = strtok($key, ' ');
				$op = strtok(' ');

				$op = strtolower($op);

				if (!in_array($op, BaseModel::$__allowedOperations))
					$op = '=';

				if ($op != 'like') {
					$param = ':' . preg_replace('/\\W/i', '', $name);
					$bindParams[$param] = $item;
				}

				$item = sprintf("%s%s %s %s", $paramPrefix, $name, $op, $op != 'like' ? $param : \Modules\Framework\Core\DB::Command()->quote($item));
			});
		}

		return $bindParams;
	}

	protected function afterLoad() {

	}

	/**
	 * * Загрузить данные модели из HTTP-запроса
	 *
	 * @param $source array POST данные
	 * @param $files данные о загруженных файлах
	 */
	public function map($source, $files) {
		$className = get_class($this);

		if (method_exists($this, 'beginMapping')) {
			$this->beginMapping();
		}

		foreach ($source as $key => $value) {
			if (!property_exists($className, $key)) {
				continue;
			}

			$this->$key = $value;
		}

		foreach ($files as $key => $file) {
			if ($file['error'] == \UPLOAD_ERR_NO_FILE)
				continue;

			$extension = strtolower(pathinfo($file['name'], \PATHINFO_EXTENSION));

			if (!in_array($extension,  Config::getInstance()->allowedFileExtensions)) {
				throw new \InvalidArgumentException("Неподдерживаемый формат файла " . $file['name']);
			}

			if (!property_exists($className, $key)) {
				throw new \InvalidArgumentException("Свойство не найдено " . $key);
			}

			if (!$file['size'] || $file['error'] != 0) {
				throw new \InvalidArgumentException("Ошибка загрузки файла " . $file['name'] . "({$file['error']})");
			}

			$filepath = Config::getInstance()->fileUploadDir . $file['name'];
			$filename = strtolower(pathinfo($file['name'], \PATHINFO_FILENAME));

			if (file_exists($filepath)) {
				$i = 1;
				while (file_exists(Config::getInstance()->fileUploadDir . "$filename($i).$extension")) $i++;
				$filepath = Config::getInstance()->fileUploadDir . "$filename($i).$extension";
			}

			if (move_uploaded_file($file['tmp_name'], $filepath)) {
				chmod($filepath, 0777);
				$this->$key = pathinfo($filepath, \PATHINFO_BASENAME);
			} else {
				$this->$key = null;
			}
		}

		if (method_exists($this, 'endMapping')) {
			$this->endMapping();
		}
	}

	/**
	 * Сохранить текущий экземпляр модели в БД
	 */
	public function save() {
		$metadata = MetadataProvider::getInstance()->getPropertyMetadata(get_class($this));

		$this->beforeSave($metadata);

		$propertyNames = array_keys((array) $metadata);

		if ($this->id) {
			$__primaryKey = static::$__primaryKey;
			$values = array_map(function($name) { return $name . " = :" . $name; },
				array_filter($propertyNames, function ($name) use ($__primaryKey) { return $name != $__primaryKey; }));

			$sql = sprintf("UPDATE `%s%s` SET %s WHERE id = :id", 
								Config::getInstance()->connection['dbPrefix'], 
								$this->getName(), 
								implode(", ", $values));
		} else {
			$values = array_map(function($name) { return ":" . $name; }, $propertyNames);

			$sql = sprintf("INSERT INTO `%s%s` (%s) VALUES (%s)", 
								Config::getInstance()->connection['dbPrefix'], 
								$this->getName(), 
								implode(", ", array_map(function ($item) { return "`$item`"; }, $propertyNames)),
								implode(", ", $values));
		}

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);

		$model = $this;
		$params = array();

		array_walk($propertyNames, function ($name) use (&$params, $model) {
			$params[$name] = $model->$name;
		});

		$db->execute($params);

		if (!$this->id) {
			$this->id = \Modules\Framework\Core\DB::Command()->lastInsertId();
		}

		$this->afterSave();
	}

	protected function beforeSave() {

	}

	protected function afterSave() {

	}

	public static function count($params = null) {
		if ($params && is_string($params) && ctype_alnum(str_replace(array('-'), '', $params))) {
			$params = array(static::$__primaryKey => $params);
		}

		$bindParams = array();

		$where = '';

		if (is_array($params)) {
			array_walk($params, function(&$item, $key) use (&$bindParams) {
				$param = ':' . reset(explode(' ', $key));
				$bindParams[$param] = $item;
				$item = sprintf("%s= %s", $key, $param);
			});

			$where = sprintf('WHERE %s', implode(" AND ", $params));
		}

		$sql = sprintf('SELECT COUNT(1) FROM `%s%s` %s', 
						Config::getInstance()->connection['dbPrefix'], 
						self::getName(), 
						$where);

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);
		$db->execute($bindParams);

		return (int) $db->fetch(\PDO::FETCH_COLUMN, 0);
	}

	public function remove() {
		$sql = sprintf("DELETE FROM `%s%s` WHERE %s = :id", 
						Config::getInstance()->connection['dbPrefix'], 
						$this->getName(), 
						static::$__primaryKey);

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);
		$db->execute(array("id" => $this->id));
	}

	/**
	 * Валидация экземпляра модели
	 * @throws \Modules\Framework\Validation\ValidateException
	 */
	public function validate() {
		$metadata =$this->getModelMetadata();

		$this->beforeValidate($metadata);

		$isValid = true;
		/* @var \Modules\Framework\Validation\Validator\AbstractValidator $validator */
		foreach ($metadata as $fieldMetadata) {
			if (!$fieldMetadata->validation)
				continue;

			foreach ($fieldMetadata->validation as $validator) {
				if (!$validator->isValid($this, $fieldMetadata)) {
					$fieldMetadata->validation->errorMessage = $validator->errorMessage;
					$isValid = false;
					break;
				}
			}
		}

		$this->afterValidate($metadata);

		if (!$isValid)
			throw new \Modules\Framework\Validation\ValidateException($metadata, "Произошли ошибки. Поправьте данные и попробуйте снова.");
	}

	protected function beforeValidate() {

	}

	protected function afterValidate() {

	}

	public function getModelMetadata() {
		if ($this->__metadata) {
			return $this->__metadata;
		}

		return $this->__metadata = MetadataProvider::getInstance()->getPropertyMetadata(get_class($this));
	}

	public function getName() {
		$className = (!(isset($this) && get_class($this) == __CLASS__)) ?
			get_called_class() :
			get_class($this);

		$metadata = MetadataProvider::getInstance()->getClassMetadata($className);

		return $metadata->tableName ?: end(explode('\\', $className));
	}
}