<?php

namespace Modules\Framework\Mvc\Controller;

use Modules\Framework\Core\Application;

/**
 * Базовый контроллер
 */
class Controller {
	protected static function redirect($url = null) {
		header(sprintf("Location: %s", $url ?: \Modules\Framework\Core\Application::getInstance()->getUrlCurrent()));
		exit;
	}

	protected static function view($name, $vars = array()) {
		$module = strtok($name, ':');
		$name = strtok(':');

		if (!$name) {
			$name = $module;

			$parts = explode('\\', get_called_class());

			$module = $parts[1];
		}

		if ($module == '~') {
			$module = \Modules\Framework\Core\Config\Config::getInstance()->mainModule;
		}

		echo new \Modules\Framework\Mvc\View\PhpView($name,
				$vars,
				Application::getInstance()->getDataDirectory() . "Views" . \DIRECTORY_SEPARATOR . $module . \DIRECTORY_SEPARATOR);
		exit;
	}

	protected static function json($vars) {
		header('Content-type: application/json');
		echo json_encode($vars);
		exit;
	}
}

?>