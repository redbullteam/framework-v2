<?php

namespace Modules\Framework\Mvc\Routing;

/**
 * Маршрутизация
 */
class Router {
	/**
	 * @var Router
	 */
	private static $instance = null;

	/**
	 * @var \Modules\Framework\Mvc\Routing\Route[]
	 */
	private $routes = array();

	protected function __construct() {
		$routes = \Modules\Framework\Core\Config\Config::getInstance()->routes;

		usort($routes, function ($prev, $next) {
			$prevPriority = $prev["priority"] ?: 0;
			$nextPriority = $next["priority"] ?: 0;

			if ($prevPriority == $nextPriority)
				return 0;

			return $prevPriority > $nextPriority ? -1 : 1;
		});

		$this->addRoutes($routes);
	}

	public function dispatch() {
		foreach ($this->routes as $route) {
			if ($route->resolve()) {
				return;
			}
		}

		throw new \Modules\Framework\Mvc\Routing\HttpException(404, "Страница не найдена");
	}

	protected static function route($pattern) {
		return new \Modules\Framework\Mvc\Routing\Route($pattern);
	}

	/**
	 * Добавить маршрут
	 *
	 * @param string $pattern
	 * @return \Modules\Framework\Mvc\Routing\Router
	 */
	public function addRoute($pattern) {
		$this->routes[] = new \Modules\Framework\Mvc\Routing\Route($pattern);

		return $this;
	}

	/**
	 * Добавить маршруты
	 *
	 * @param array $routes
	 * @return \Modules\Framework\Mvc\Routing\Router
	 */
	public function addRoutes(array $routes) {
		foreach ($routes as $route) {
			$pattern = isset($route["pattern"]) ? $route["pattern"] : '';
			$method = isset($route["method"]) ? $route["method"] : null;
			$module = isset($route["module"]) ? $route["module"] : null;
			$controller = isset($route["controller"]) ? $route["controller"] : null;
			$action = isset($route["action"]) ? $route["action"] : '';
			$defaults = isset($route["defaults"]) ? $route["defaults"] : array();

			$this->routes[] = self::route($pattern)
								->via($method)
								->to($module, $controller, $action)
								->with($defaults);
		}

		return $this;
	}

	/**
	 * @static
	 * @return Router
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new Router();
		}

		return self::$instance;
	}
}
