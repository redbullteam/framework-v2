<?php 

namespace Modules\Framework\Mvc\Routing;

use Modules\Framework\Core\Application;
use Modules\Framework\Core\ModuleSystem\ModuleManager;

/**
 * Маршрутизация
 */
class Route {
	/**
	 * @var string[]
	 */
	public $method = array('head', 'get', 'post', 'put', 'delete');
	/**
	 * @var string
	 */
	public $pattern;
	/**
	 * @var string
	 */
	public $module;
	/**
	 * @var string
	 */
	public $controller;
	/**
	 * @var string
	 */
	public $action;

	/**
	 * @var string[]
	 */
    private $defaults = array();

    /**
     * @param string $pattern
     * Шаблон URL запроса
     * Нарпимер:
     * "news/page/:page" - Роут сработает для страничек:
     * http://sitename.com/news/page/1
     * http://sitename.com/news/page/2
     * и т.д.
     * В результате будет вызван метод контроллера, определлый в to(), с параметром page = 1 [2 и т.д.]
     */
    function __construct($pattern) {
		$this->pattern = $this->convertPatternToRegex($pattern);
	}

    /**
	 * Задает ограничение принимаемых HTTP-методов
	 *
     * @param string[] Список принимаемых HTTP-методов
     * @return Route
     */
    public function via($method) {
		if ($method != null)
			$this->method = func_get_args();

        return $this;
    }

	/**
	 * Задает имя контроллера (класса) и его действие (имя соответствующего метода)
	 *
	 * @param string $module Имя модуля
	 * @param string $controller Имя класса контроллера (без неймспейса)
	 *
	 * @param string $action [optional] Имя вызываемого метода.
	 * Если метод не задан, будет вызван методот, соответствующий названию HTTP-методу запроса (Get, Post, Put, Delete)
	 *
	 * @return Route
	 */
    public function to($module, $controller, $action = null) {
		$this->module = $module;
		$this->controller = $controller;
		$this->action = $action;

		return $this;
	}

	/**
	 * @param $defaults
	 * @return Route
	 */
	public function with($defaults) {
		$this->defaults = $defaults;

		return $this;
	}

	/**
	 * Переводит шаблон запроса в регулярное выражение
	 *
	 * @param string $pattern
	 * @return string
	 */
	private function convertPatternToRegex($pattern) {
		return "/^" . preg_replace(array('/\[([^\]]+)\]/', '/:(\w+)/i', '/\//'), array('(?:\\1)?', '(?P<\\1>[^/]+)', '\\/'), $pattern) . "$/i";
	}

	/**
	 * Оставляет только именованные группы из результатов выполнения регулярного выражения
	 *
	 * @param $matches
	 * @return string
	 */
	private function convertMatchesToParams($matches) {
		$result = array();

		foreach ($matches as $k => $v) {
			if (is_string($k))
				$result[$k] = $v;
		}

		return $result;
	}

	/**
	 * @throws \Exception
	 * @return boolean Возвращает true, если текущий адрес соответствует заданному шаблону и HTTP-методов
	 * входит в число разрешенных
	 */
	public function resolve() {
		$httpMethod = strtolower($_SERVER['REQUEST_METHOD']);

		// http метод не поддерживается
		if (!in_array($httpMethod, $this->method)) {
			return false;
		}

		$isDebug = Application::getInstance()->isDebugMode();

		if (preg_match($this->pattern, \Modules\Framework\Utils\Url::getInstance()->getUrlPath(), $matches)) {
			$module = isset($matches['module']) ? $matches['module'] : $this->module;
			$controllerClassName = isset($matches['controller']) ? $matches['controller'] : $this->controller;
			$action = isset($matches['action']) ? $matches['action'] : ($this->action ?: $httpMethod);

			if ($module && ModuleManager::getInstance()->isModuleInstalled($module)) {
				$controllerClassName = "\\Modules\\$module\\Controllers\\$controllerClassName";
			} else {
				$controllerClassName = \Modules\Framework\Core\Config\Config::getInstance()->mvc['availableControllers'][$controllerClassName];

				$namespace = explode('\\', $controllerClassName);
				$module = $namespace[1];
			}

			// контроллер не найден
			if (!class_exists($controllerClassName)) {
				if ($isDebug)
					throw new \Exception(sprintf("Контроллер %s не найден", $controllerClassName));

				return false;
			}

			$controller = new $controllerClassName();

			// действие контроллера не найдено
			if (!method_exists($controller, $action)) {
				if ($isDebug)
					throw new \Exception(sprintf("Действие %s контроллера %s не найдено", $action, $this->controller));

				return false;
			}

			$reflectionMethod = new \ReflectionMethod($controller, $action);

			//TODO: add file mapping
			$args = array_merge($this->convertMatchesToParams($matches), $_REQUEST);
			$pass = array();

			foreach($reflectionMethod->getParameters() as $param) {
				$className = \Modules\Framework\Utils\Reflection::resolveParameterClassName($param);
				$paramName = $param->getName();

				if (isset($args[$paramName]) || $className) {
					$pass[$paramName] = $className ?
								\Modules\Framework\Utils\Map::arrayToObject(isset($args[$paramName]) ? $args[$paramName] : $args, $className) :
								$args[$param->getName()];
				} else {
					$pass[$paramName] = isset($this->defaults[$paramName])
											? $this->defaults[$paramName]
											: $param->getDefaultValue();
				}
			}

			$urlHelper = \Modules\Framework\Utils\Url::getInstance();
			$urlHelper->module = $module;
			$urlHelper->controller = end(explode('\\', $controllerClassName));
			$urlHelper->action = $action;
			$urlHelper->params = $pass;

			if (method_exists($controller, 'onActionExecuting')) {
				$onActionExecuting = new \ReflectionMethod($controller, 'onActionExecuting');
				$onActionExecuting->invokeArgs($controller, $pass);
			}

			if (method_exists($controller, 'onActionExecuted')) {
				register_shutdown_function(function () use ($controller, $pass) {
					$onActionExecuted = new \ReflectionMethod($controller, 'onActionExecuted');
					$onActionExecuted->invokeArgs($controller, $pass);
				});
			}

			echo $reflectionMethod->invokeArgs($controller, $pass);

			return true;
		}

		return false;
	}
}

?>