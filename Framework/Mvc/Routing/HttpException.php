<?php 

namespace Modules\Framework\Mvc\Routing;

/**
 * HTTP исключение
 * Используется в случаях, если не найден контроллер или его действие
 */
class HttpException extends \Exception {
	/**
	 * @var int
	 */
	public $code;

	/**
	 * @param int $code HTTP-код ошибки
	 * @param string $message Сообщение
	 */
	public function __construct($code, $message) {
		$this->code = $code;

		parent::__construct($message);
	}
}

?>