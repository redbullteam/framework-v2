<?php

namespace Modules\Framework\Mvc\View\Helpers;

class Section {
	const SET = 0;
	const APPEND = 1;

  	/*
  	 * @var Modules\Framework\Mvc\View\PhpView
  	 */
	private $view;
	protected $__sectionStack = array();

	public function __construct($view) {
		$this->view = $view;
	}

	public function begin($name, $mode = Section::SET) {
		$this->__sectionStack[] = array($name, $mode);
		ob_start();
	}

	public function append($name) {
		$this->begin($name, Section::APPEND);
	}

	public function end() {
		list($name, $mode) = array_pop($this->__sectionStack);

		switch ($mode) {
			case Section::SET:
				$this->view->assign($name, ob_get_clean());
				break;
			case Section::APPEND:
				$this->view->assign($name, $this->view->get($name) . ob_get_clean());
				break;
		}
	}
}
