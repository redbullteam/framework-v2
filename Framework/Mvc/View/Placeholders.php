<?php

namespace Modules\Framework\Mvc\View;

class Placeholders {
	/**
	 * @var Placeholders
	 */
	private static $instance = NULL;

	private $placeholders = array();

	private function __construct() {

	}

	public static function Add($name, $value) {
		if (!isset(self::Instance()->placeholders[$name])) {
			self::Instance()->placeholders[$name] = array( $value );
		} else {
			if (!in_array($value, self::Instance()->placeholders[$name]))
				array_push(self::Instance()->placeholders[$name], $value);
		}
	}

	public static function Set($name, $value) {
		self::Instance()->placeholders[$name] = array( $value );
	}

	public static function Get($name) {
		return implode('', self::Instance()->placeholders[$name] ?: array());
	}

	/**
	 * @static
	 * @return Placeholders
	 */
	private static function Instance() {
		if(self::$instance == null) {
			self::$instance = new Placeholders();
		}

		return self::$instance;
	}
}

?>