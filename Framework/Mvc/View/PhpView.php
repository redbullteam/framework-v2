<?php

namespace Modules\Framework\Mvc\View;

use Modules\Framework\Core\Application;

/**
 * Базовое представление
 */
class PhpView {
	const BASEEXT = ".php";

	private $__filename;
	private $__parent;
	private $__params;

	public function __construct($name, $params = null, $baseDir = null) {
		$module = strtok(str_replace('/', \DIRECTORY_SEPARATOR, $name), ':');
		$name = strtok(':');

		if (!$name) {
			$name = $module;
			$module = null;
		}

		if ($module == '~') {
			$module = \Modules\Framework\Core\Config\Config::getInstance()->mainModule;
		}

		$this->__params = $params;

		$this->__filename = ($baseDir == null || $module || strpos($name, "/") === 0  ?
									Application::getInstance()->getDataDirectory() . "Views" :
									$baseDir) // basedir
								. ($module ? \DIRECTORY_SEPARATOR . $module : '') // module
								. ($name ? \DIRECTORY_SEPARATOR . $name : '') // view
								. PhpView::BASEEXT;

		$this->registerHelpers();
	}

	public function assign($key, $value) {
		if (is_array($key) || is_callable($key)) {
			$this->__params = $key;
		} else {
			$this->__params[$key] = $value;
		}

		return $this;
	}

	public function get($key) {
		return isset($this->__params[$key]) ? $this->__params[$key] : '';
	}

	protected function registerHelpers() {
		$this->__section = new \Modules\Framework\Mvc\View\Helpers\Section($this);
		$this->__url = \Modules\Framework\Utils\Url::getInstance();
	}

	protected function setBaseParams() {
		$this->mainModule = \Modules\Framework\Core\Config\Config::getInstance()->mainModule;
		$this->debugEnabled = \Modules\Framework\Core\Application::getInstance()->isDebugMode();

		return $this;
	}

	protected function setParams($params) {
		if (is_callable($params)) {
			$params = $params();
		}

		if (empty($params)) {
			return;
		}

		foreach($params as $k => $v) {
			$this->$k = $v;
		}
	}

	public function partial($name, $params = null) {
		$view = new PhpView($name, $params ?: $this->__params, dirname($this->__filename));
		$view->__section = $this->__section;

		return $view;
	}

	public function extend($name) {
		$this->__parent = $name;
	}

	public function __toString() {
		try {
			if (!file_exists($this->__filename)) {
				throw new \Exception(sprintf("Представление %s не найдено", $this->__filename));
			}

			$this->setBaseParams();
			$this->setParams($this->__params);

			ob_start();

			extract(getPublicObjectVars($this));

			require realpath($this->__filename);

			$result = ob_get_clean();

			return $this->__parent ?
				(string) new PhpView($this->__parent, $this->__params, dirname($this->__filename)) :
				$result;
		}
		catch(\Exception $e)
		{
			\Modules\Framework\Utils\Logger::Add($e);
			return '';
		}
	}

	public function __get($name) {
		return $this->get($name);
	}
}

function getPublicObjectVars($obj) {
	return get_object_vars($obj);
}