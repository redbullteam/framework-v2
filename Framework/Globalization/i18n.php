<?php

namespace Modules\Framework\Globalization;

use Modules\Framework\Core\Config\Config;

class i18n {
	private static $_current;

	private static $_langs = array(
		'en' => 'en',
		'en_US' => 'en',
		'en_UK' => 'en',
		'ru' => 'ru',
		'ru_RU' => 'ru'
	);

	/**
	 * @return object
	 */
	public static function current() {
		if (self::$_current != null) {
			return self::$_current;
		}

		$acceptLanguages = self::getBrowserAcceptLanguages();

		//$acceptLanguages = array();

		foreach ($acceptLanguages as $lang => $dummy) {
			$lang = self::$_langs[$lang] ?: null;

			if ($lang == null)
				continue;

			self::$_current = self::loadDictionary($lang);

			if (self::$_current !== false)
				return self::$_current;
		}

		return self::$_current = self::loadDictionary(Config::getInstance()->defaultLanguage);
	}

	public static function loadDictionary($locale) {
		$filename = __DIR__ . "/Dictionaries/$locale.php";

		if (!file_exists($filename))
			return false;

		return (object) require $filename;
	}

	public static function getBrowserAcceptLanguages() {
		$httpLanguages = getenv('HTTP_ACCEPT_LANGUAGE');

		if (empty($httpLanguages) && array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER)) {
			$httpLanguages = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		}

		$languages = array();
		if (empty($httpLanguages)) {
			return $languages;
		}

		$accepted = preg_split('/,\s*/', $httpLanguages);

		foreach ($accepted as $accept) {
			$match = null;
			$result = preg_match('/^([a-z]{1,8}(?:[-_][a-z]{1,8})*)(?:;\s*q=(0(?:\.[0-9]{1,3})?|1(?:\.0{1,3})?))?$/i',
				$accept, $match);

			if ($result < 1) {
				continue;
			}

			if (isset($match[2]) === true) {
				$quality = (float)$match[2];
			} else {
				$quality = 1.0;
			}

			$countries = explode('-', $match[1]);
			$region = array_shift($countries);

			$country2 = explode('_', $region);
			$region = array_shift($country2);

			foreach ($countries as $country) {
				$languages[$region . '_' . strtoupper($country)] = $quality;
			}

			foreach ($country2 as $country) {
				$languages[$region . '_' . strtoupper($country)] = $quality;
			}

			if ((isset($languages[$region]) === false) || ($languages[$region] < $quality)) {
				$languages[$region] = $quality;
			}
		}

		return $languages;
	}
}
