<?php

namespace Modules\Framework\Utils;

use Modules\Framework\Utils\Arrays;

class DynamicObject extends \ArrayObject {
	/*
	 * @param string $key Ключ добавляемого свойства или массив свойств
	 * @param mixed $value Значение добавляемого свойства или значение
	 */
	public function add($key, $value = null) {
		if (is_array($key)) {
			$this->exchangeArray(Arrays::arrayMergeRecursiveOverrule($this->getArrayCopy(), $key));

			return $this;
		}

		if (!is_array($value)) {
			$this[$key] = $value;
		} else {
			$this[$key] = Arrays::arrayMergeRecursiveOverrule(isset($this[$key]) ? $this[$key] : array(), $value);
		}

		return $this;
	}

	public function __set($name, $val) {
		$this->add($name, $val);
	}

	public function __get($name) {
		return isset($this[$name]) ? $this[$name] : null;
	}
}