<?php

namespace Modules\Framework\Utils;

class Mailer {
	private $from;
	private $fromEmail;

	private $to;

	private $subject;
	private $text;
	private $body;

	private $headers;

	private $isHtml;

	public function __construct($isHtml = false) {
		$this->from = \Modules\Framework\Core\Config\Config::getInstance()->mailer['fromName'];
		$this->fromEmail = \Modules\Framework\Core\Config\Config::getInstance()->mailer['formEmail'];

		$this->to = array();
		$this->headers = array();

		$this->isHtml = $isHtml;
	}

	public static function getInstance($isHtml = false) {
		return new Mailer($isHtml ?: false);
	}

	public function send() {
		$headers   = array();

		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: ' . ($this->isHtml ? 'text/html' : 'text/plain') . '; charset=utf-8';

		$from = "=?UTF-8?B?".base64_encode($this->from)."?=";
		$headers[] = "From: $from <{$this->fromEmail}>";

		$subject = "=?UTF-8?B?".base64_encode($this->subject)."?=";
		$headers[] = "Subject: $subject";

		$headers[] = "X-Mailer: PHP/" . phpversion();

		foreach ($this->headers as $header)
			$headers[] = $header;

		$to = is_array($this->to) ? implode(", ", $this->to) : $this->to;

		if ($this->isHtml) {
			$message = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>' .
				$this->subject . '</title></head><body>' .
				$this->body . '</body></html>';

			/*
//unique boundary
$boundary = uniqid("HTMLDEMO");

//tell e-mail client this e-mail contains//alternate versions
$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";

//plain text version of message
$body = "--$boundary\r\n" .
    "Content-Type: text/plain; charset=ISO-8859-1\r\n" .
    "Content-Transfer-Encoding: base64\r\n\r\n";
$body .= chunk_split(base64_encode("This is the plain text version!"));

//HTML version of message
$body .= "--$boundary\r\n" .
    "Content-Type: text/html; charset=ISO-8859-1\r\n" .
    "Content-Transfer-Encoding: base64\r\n\r\n";
$body .= chunk_split(base64_encode("This the <b>HTML</b> version!"));
			 */
		} else {
			$message = $this->body;
		}

		return mail($to, $subject, $message, implode("\r\n", $headers));
	}

	public function addHeader($header) {
		$this->headers[] = $header;

		return $this;
	}

	public function setFrom($from, $fromEmail = null) {
		if (!$fromEmail) {
			$this->fromEmail = $from;
		} else {
			$this->from = $from;
			$this->fromEmail = $fromEmail;
		}

		return $this;
	}

	public function setText($message) {
		$this->text = $message;

		return $this;
	}

	public function setBody($message) {
		$this->isHtml = true;

		$this->body = $message;
		if (!$this->text)
			$this->text = strip_tags($message);

		return $this;
	}

	public function setSubject($subject) {
		$this->subject = $subject;

		return $this;
	}

	public function setTo($to) {
		$this->to = $to;

		return $this;
	}
}

?>