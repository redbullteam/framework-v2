<?php

namespace Modules\Framework\Utils;

class Map {
	public static function objectToObject($object, $className, $strict = true) {
		throw new \Exception("Not implemented yet");
	}

	public static function arrayToObject($array, $className = 'stdClass', $strict = true) {
		if (!is_array($array)) {
			return $array;
		}

		$object = new $className();

		if (count($array) > 0) {
			if (method_exists($object, 'beginMapping')) {
				$object->beginMapping();
			}

			foreach ($array as $name => $value) {
				if (!empty($name)) {
					if (method_exists($object, 'set'.$name)) {
						$object->{'set'.$name}($value);
					} else {
						if ($strict) {
							if (property_exists($className, $name)) {
								$object->$name = self::arrayToObject($value);
							}
						} else {
							$object->$name = self::arrayToObject($value);
						}
					}
				}
			}

			if (method_exists($object, 'endMapping')) {
				$object->endMapping();
			}

			return $object;
		} else {
			return null;
		}
	}
}


?>