<?php

namespace Modules\Framework\Utils;

class TextUtils {
	/**
	 * Переводит строку в транслит
	 *
	 * @param string $str
	 * @return string
	 */
	public static function translit($str)
	{
		$tr = array(
			"А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
			"Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
			"Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
			"О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
			"У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
			"Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
			"Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
			"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
			"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
			"ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
		);

		$str = strtolower(strtr($str,$tr));

		return preg_replace(array('/[^a-zA-Z0-9_-]/s', '/-+/'), array('-', '-'), $str);
	}

	/**
	 * Создает валидную строку для URL
	 * Например, если передать заголовок "!!! Наша первая статья !!!"
	 * на выходе получим "nasha-pervaya-statya", которую можно будет использовать
	 * в URL вида http://site.com/articles/nasha-pervaya-statya для идентификации
	 *
	 * @param string $source
	 * @return string
	 */
	public static function makeAlias($source) {
		if (!$source)
			return "";

		// удаляем служебные символы в начале и конце строки, а внутри заменяем их на символы "-"
		return preg_replace(array("/^\W+|\W+$/", "/\W/"), array("", "-"), self::translit($source));
	}
}