<?php

namespace Modules\Framework\Utils;

/**
 * Регистратор ошибок
 *
 * Пишет ошибки, произошедшие на сайте, в папку Log в корне сайта.
 * Это позволяет сохранить сведения об ошибках для дальнейшего их выявления
 * в исходных кодах, при этом не показывая пользователю ненужную ему
 * техническую информацию, заменив ее оформленной страничкой о проблемах на
 * сайте и ориентировочным временем их решения
 */
class Logger {
	/**
	 * @var Logger
	 */
	private static $instance = null;

	/**
	 * @var string
	 */
	private $dirname;

	protected function __construct() {
		$this->dirname = \Modules\Framework\Core\Application::getInstance()->getDataDirectory() . "Log" . \DIRECTORY_SEPARATOR;

		if (!file_exists($this->dirname)) {
			mkdir($this->dirname, 0777, true);
		}
	}

	protected function Write($message) {
		if ($message instanceof \Exception) {
			$message = sprintf("Дата: %s
Ссылка: %s
Исключение: %s
Файл: %s
Номер строки: %d
Трассировка: %s",
				date("Y-m-d H:i:s"),
				$_SERVER["REQUEST_URI"],
				$message->getMessage(),
				$message->getFile(),
				$message->getLine(),
				print_r($message->getTraceAsString(), true));
		} else if (is_string($message)) {
			$params = func_get_args();
			$message = count($params) > 1
				? call_user_func_array('sprintf', $params)
				: $message;
		}

		file_put_contents($this->dirname .  date("Y-m-d") . '.log', $message . "\n\n", \FILE_APPEND | \LOCK_EX);
	}

	public static function Add() {
		call_user_func_array(array(self::Instance(), 'Write'), func_get_args());
	}

	public static function ChangeDir($dir) {
		self::Instance()->dirname = $dir;
	}

	/**
	 * @static
	 * @return Logger
	 */
	private static function Instance() {
		if(self::$instance == NULL) {
			self::$instance = new Logger();
		}

		return self::$instance;
	}
}