<?php

namespace Modules\Framework\Utils;

class Reflection {
	public static function resolveParameterClassName(\ReflectionParameter $reflectionParameter) {
		$className = null;

		try {
			$reflectionClass = $reflectionParameter->getClass();

			if ($reflectionClass != null)
				return $reflectionClass->name;
		} catch (\Exception $exception) {}

		if ($reflectionParameter->isArray()) {
			return null;
		}

		$reflectionString = $reflectionParameter->__toString();
		$searchPattern = '/^Parameter \#' . $reflectionParameter->getPosition() . ' \[ \<required\> ([A-Za-z]+) \$' . $reflectionParameter->getName() . ' \]$/';

		$matchResult = preg_match($searchPattern, $reflectionString, $matches);

		if (!$matchResult) {
			return null;
		}

		return array_pop($matches);
	}
}