<?php

namespace Modules\Framework\Utils;

class Arrays {
	/**
	 * Merges two arrays recursively and "binary safe" (integer keys are overridden as well), overruling similar values in the first array ($firstArray) with the values of the second array ($secondArray)
	 * In case of identical keys, ie. keeping the values of the second.
	 *
	 * @param array $firstArray First array
	 * @param array $secondArray Second array, overruling the first array
	 * @param boolean $dontAddNewKeys If set, keys that are NOT found in $firstArray (first array) will not be set. Thus only existing value can/will be overruled from second array.
	 * @param boolean $emptyValuesOverride If set (which is the default), values from $secondArray will overrule if they are empty (according to PHP's empty() function)
	 * @return array Resulting array where $secondArray values has overruled $firstArray values
	 */
	static public function arrayMergeRecursiveOverrule(array $firstArray, array $secondArray, $dontAddNewKeys = FALSE, $emptyValuesOverride = TRUE) {
		reset($secondArray);
		while (list($key, $value) = each($secondArray)) {
			if (array_key_exists($key, $firstArray) && is_array($firstArray[$key])) {
				if (is_array($secondArray[$key])) {
					$firstArray[$key] = self::arrayMergeRecursiveOverrule($firstArray[$key], $secondArray[$key], $dontAddNewKeys, $emptyValuesOverride);
				} else {
					$firstArray[$key] = $secondArray[$key];
				}
			} else {
				if ($dontAddNewKeys) {
					if (array_key_exists($key, $firstArray)) {
						if ($emptyValuesOverride || !empty($value)) {
							$firstArray[$key] = $value;
						}
					}
				} else {
					if ($emptyValuesOverride || !empty($value)) {
						$firstArray[$key] = $value;
					}
				}
			}
		}
		reset($firstArray);
		return $firstArray;
	}

	/**
	 * Returns the values from a single column of the input array, identified by the columnKey.
	 *
	 * Optionally, you may provide an indexKey to index the values in the returned array by the values from the indexKey column in the input array.
	 *
	 * @param array[]         $input                 A multi-dimensional array (record set) from which to pull a column of values.
	 * @param int|string      $columnKey             The column of values to return. This value may be the integer key of the column you wish to retrieve, or it may be the string key name for an associative array.
	 * @param int|string      $indexKey              The column to use as the index/keys for the returned array. This value may be the integer key of the column, or it may be the string key name.
	 *
	 * @return mixed[]
	 */
	static public function arrayColumn ($input, $columnKey, $indexKey = null) {
		if (!is_array($input)) {
			return false;
		}

		if (\is_callable('array_column', false))
			return \array_column(array_map(function ($item) { return (array) $item; }, $input), $columnKey, $indexKey);

		if ($indexKey === null) {
			foreach ($input as $i => &$in) {
				if (is_array($in) && isset($in[$columnKey])) {
					$in = $in[$columnKey];
				} else {
					unset($input[$i]);
				}
			}
		} else {
			$result = array();

			foreach ($input as $i => $in) {
				if (is_array($in) && isset($in[$columnKey])) {
					if (isset($in[$indexKey])) {
						$result[$in[$indexKey]] = $in[$columnKey];
					} else {
						$result[] = $in[$columnKey];
					}
					unset($input[$i]);
				}

				if (is_object($in) && isset($in->$columnKey)) {
					if (isset($in->$indexKey)) {
						$result[$in->$indexKey] = $in->$columnKey;
					} else {
						$result[] = $in->$columnKey;
					}
				}
			}

			$input = &$result;
		}

		return $input;
	}
}
