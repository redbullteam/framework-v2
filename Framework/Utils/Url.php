<?php

namespace Modules\Framework\Utils;

class Url {
	/**
	 * @var Url
	 */
	private static $instance = null;

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @var string
	 */
	public $module;

	/**
	 * @var string
	 */
	public $controller;

	/**
	 * @var string
	 */
	public $action;

	/**
	 * @var array
	 */
	public $params;

	/**
	 * @return string
	 */
	public function getUrlPath() {
		if ($this->url)
			return $this->url;

		return $this->url = isset($_SERVER['REQUEST_URI']) ?
			trim(reset(explode('?', $_SERVER['REQUEST_URI'])), '/') :
			'';
	}

	/**
	 * @return string
	 */
	public function getUrlCurrent() {
		return "/" . $this->getUrlPath();
	}

	/**
	 * @return string
	 */
	public function getRawUrl() {
		return $_SERVER['REQUEST_URI'];
	}

	/**
	 * @return string
	 */
	public function getUrlBackward() {
		if (isset($_GET['redirectUrl']) && $_GET['redirectUrl'])
			return $_GET['redirectUrl'];

		$url = $this->getUrlPath();

		return "/" . substr($url, 0, strrpos($url, "/"));
	}

	/**
	 * @param string $action
	 * @param mixed $controller
	 * @param mixed $module
	 * @param mixed $params
	 *
	 * @return string
	 */
	public function action($action, $controller = null, $module = null, $params = null) {
		if (is_array($module)) {
			$params = $module;
			$module = $this->module;
		}

		if (is_array($controller)) {
			$params = $controller;
			$module = $this->module;
			$controller = $this->controller;
		}

		if (!$controller) {
			$controller = $this->controller;
		}

		if (!$module) {
			$module = $this->module;
		}

		return sprintf('/%s/%s/%s%s', $module, $controller, $action, !empty($params) ? '?' . http_build_query($params) : '');
	}

	public function modify(array $params = array()) {
		parse_str($_SERVER['QUERY_STRING'], $query_string);

		$query_string = array_merge($query_string, $params);

		return sprintf("/%s?%s", $this->getUrlPath(), http_build_query($query_string));
	}

	/**
	 * @static
	 * @return Url
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}
