<?php

namespace Modules\Framework\Annotations;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Ignore implements \Modules\Framework\Core\Metadata\IMetadata {
	public function __construct(array $values) {

	}

	public function getMetadataKey() {
		return "ignore";
	}
}
