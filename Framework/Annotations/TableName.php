<?php

namespace Modules\Framework\Annotations;

use Modules\Framework\Globalization\i18n;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class TableName implements \Modules\Framework\Core\Metadata\IMetadata {
	public $text;

	public function __construct(array $values) {
		$this->text = $values['value'];
	}

	public function __toString() {
		return $this->text;
	}

	public function getMetadataKey() {
		return "tableName";
	}
}
