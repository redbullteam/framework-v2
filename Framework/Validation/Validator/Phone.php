<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Phone extends DataType {
	public $dataType = DataType::PHONENUMBER;

	public $pattern = '/^[\d\(\)\-\+\s]{5,19}$/';

	public function __construct(array $values = array()) {
		$this->pattern = $values['value'] ?: $this->pattern;
	}

	function isValid($model, $metadata) {
		return $this->isValid = (filter_var(
									$model->{$metadata->fieldName},
									FILTER_VALIDATE_REGEXP,
									array(
										'options' => array(
											'regexp' => $this->pattern
										)
									)) != false);
	}
}
