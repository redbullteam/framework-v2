<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Required extends AbstractValidator {
	public $errorMessage;

	public function __construct(array $values) {
		$this->errorMessage = \Modules\Framework\Globalization\i18n::current()->required;
	}

	function isValid($model, $metadata) {
		return $this->isValid = !!$model->{$metadata->fieldName};
	}
}
