<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Email extends DataType {
	public $dataType = DataType::EMAIL;

	public function __construct(array $values = array()) {
	}

	function isValid($model, $metadata) {
		return $this->isValid = (filter_var($model->{$metadata->fieldName}, FILTER_VALIDATE_EMAIL) !== false);
	}
}
