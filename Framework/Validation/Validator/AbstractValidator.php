<?php

namespace Modules\Framework\Validation\Validator;

abstract class AbstractValidator implements \Modules\Framework\Core\Metadata\IMetadata {
	public $errorMessage;
	public $isValid = true;

	function isValid($model, $metadata) {
		return $this->isValid = true;
	}

	/*
	 * @param BaseModel $model
	 * @param DynamicMetadata $metadata
	 */
	function validate($model, $metadata) {
		$this->isValid($model, $metadata);
	}

	public function getMetadataKey() {
		return "validation." . strtolower(end(explode('\\', get_class($this))));
	}
}
