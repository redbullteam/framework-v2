<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Unique extends AbstractValidator {
	public $errorMessage;

	public function __construct(array $values) {
		$this->errorMessage = \Modules\Framework\Globalization\i18n::current()->unique;
	}

	function isValid($model, $metadata) {
		$sql = sprintf("SELECT 1 FROM `%s%s` WHERE %s = :%s AND %s != :__primaryKey",
                        \Modules\Framework\Core\Config\Config::getInstance()->connection['dbPrefix'],
                        $model->getName(),
                        $metadata->fieldName,
                        $metadata->fieldName,
                        $model::$__primaryKey);

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);

		$params = array("__primaryKey" => $model->{$model::$__primaryKey});
		$params[$metadata->fieldName] = $model->{$metadata->fieldName};

		$db->execute($params);

		if ($db->fetch(\PDO::FETCH_COLUMN, 0))
			$this->isValid = false;

		return $this->isValid;
	}
}
