<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Range extends DataType {
	public $dataType = DataType::NUMBER;

	public $minimum;
	public $maximum;

	public function __construct(array $values) {
		$this->minimum = $values['minimum'];
		$this->maximum = $values['maximum'];
	}

	function isValid($model, $metadata) {
		return $this->isValid = (filter_var(
									$model->{$metadata->fieldName},
									FILTER_VALIDATE_INT,
									array(
										'options' => array(
											'min_range' => $this->minimum,
											'max_range' => $this->maximum
										)
									)) !== false);
	}
}
