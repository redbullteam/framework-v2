<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class DataType extends AbstractValidator {
	const BOOL 			= "bool";
	const BOOLEAN 		= "boolean";

	const INT 			= "integer";
	const INTEGER 		= "integer";

	const FLOAT 		= "number";
	const NUMBER 		= "number";

	const DATE 			= "date";
	const DATETIME 		= "date";

	const PHONE 		= "phone";
	const PHONENUMBER 	= "phone";

	const TEXT 			= "text";
	const MULTILINETEXT = "multilinetext";
	const HTML			= "html";
	const HIDDEN 		= "hidden";

	const EMAIL 		= "email";
	const EMAILADDRESS 	= "email";

	const PASSWORD		= "password";

	const URL			= "url";

	const UPLOAD		= "upload";

	public $dataType;
	public $errorMessage;

	public function __construct(array $values) {
		$this->dataType = $values['value'] ?: DataType::TEXT;
		$this->errorMessage = \Modules\Framework\Globalization\i18n::current()->invalidFormat;
	}

	function isValid($model, $metadata) {
		$value = $model->{$metadata->fieldName};

		switch ($this->dataType) {
			case DataType::BOOLEAN:
				return $this->isValid = (filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null);
				break;

			case DataType::INTEGER:
				return $this->isValid = (filter_var($value, FILTER_VALIDATE_INT) !== false);

			case DataType::NUMBER:
				return $this->isValid = (filter_var($value, FILTER_VALIDATE_FLOAT) !== false);

			case DataType::DATE:
				$validator = new DateTime(array('format' => DateTime::RU_DATE));
				return $validator->isValid($model, $metadata);

			case DataType::DATETIME:
				$validator = new DateTime();
				return $validator->isValid($model, $metadata);

			case DataType::PHONENUMBER:
				$validator = new Phone();
				return $validator->isValid($model, $metadata);

			case DataType::EMAILADDRESS:
				$validator = new Email();
				return $validator->isValid($model, $metadata);

			case DataType::URL:
				return $this->isValid = (filter_var($value, FILTER_VALIDATE_URL) !== false);
		}

		return true;
	}

	function __toString() {
		return $this->dataType;
	}
}
