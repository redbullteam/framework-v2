<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class DateTime extends DataType {
	const RU_DATE = 'd.m.Y';
	const RU_DATETIME = 'd.m.Y H:i:s';

	public $dataType = DataType::DATETIME;

	public $format = DateTime::RU_DATETIME;

	public function __construct(array $values = array()) {
		$this->format = $values['value'];
	}

	function isValid($model, $metadata) {
		try {
			\DateTime::createFromFormat($this->format, $model->{$metadata->fieldName});
			return $this->isValid = true;
		} catch (\Exception $e) {
			return $this->isValid = false;
		}
	}
}
