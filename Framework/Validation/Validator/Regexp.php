<?php

namespace Modules\Framework\Validation\Validator;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Regexp extends AbstractValidator {
	public $pattern;
	public $errorMessage;

	public function __construct(array $values) {
		$this->pattern = $values['value'];
		$this->errorMessage = \Modules\Framework\Globalization\i18n::current()->invalidFormat;
	}

	function isValid($model, $metadata) {
		return $this->isValid = (filter_var(
									$model->{$metadata->fieldName},
									FILTER_VALIDATE_REGEXP,
									array(
										'options' => array(
											'regexp' => $this->pattern
										)
									)) != false);
	}
}
