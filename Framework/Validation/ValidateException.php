<?php 

namespace Modules\Framework\Validation;

/**
 * Ошибка валидации свойства модели
 */
class ValidateException extends \Exception {
	/**
	 * @var string
	 */
	public $fields;

	/**
	 * @param string[] $fields Имена свойств модели
	 * @param string $message Сообщение
	 */
	public function __construct($fields, $message) {
		$this->fields = $fields;

		parent::__construct($message);
	}
}

?>