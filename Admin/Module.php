<?php

namespace Modules\Admin;

use Modules\Framework\Core\ModuleSystem\ModuleManager;

class Module extends \Modules\Framework\Core\ModuleSystem\Module {
	public $dependencies = array("Framework", "Membership");
	public $autoInstall = true;

	public function init() {
		ModuleManager::getInstance()->registerAnnotationsNamespace('Modules\\Admin\\Annotations');
	}
}