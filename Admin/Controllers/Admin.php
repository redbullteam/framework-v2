<?php

namespace Modules\Admin\Controllers;

use Modules\Framework\Mvc\Model\BaseModel;
use Modules\Framework\Globalization\i18n;
use Modules\Framework\Core\Config\Config;
use Modules\Framework\Mvc\Routing\HttpException;
use Modules\Framework\Validation\ValidateException;
use Modules\Framework\Core\Metadata\MetadataProvider;

/**
 * Контроллер системы администрирования
 */
class Admin extends \Modules\Framework\Mvc\Controller\Controller {
	public function get($model, $id = null, $metadata = null) {
		if (!$model) {
			throw new HttpException(404, "Не задан обязательный параметр: model");
		}

		$className = Config::getInstance()->mvc['availableModels'][$model];

		if (!class_exists($className)) {
			throw new \Exception(sprintf("Модель %s не найдена", $className));
		}

		$fullMetadata = MetadataProvider::getInstance()->getFullMetadata($className);

		$single = $id || $id === "new";

		$param = array(
			"pageTitle" => $fullMetadata->class->display,
			"fields" => $metadata ?: $fullMetadata->fields,
			"new" => $id == "new",
		    "modelPersist" => $fullMetadata->class->persist
		);

		if ($single) {
			/* @var BaseModel $instance */
			$instance = new $className();
			$param["model"] = is_numeric($id) ?
				$instance->Load($id) :
				$instance;

			if ($id != "new" && !$instance->id)
				throw new HttpException(404, "Элемент не найден");
		} else {
			$filter = new Filter();
			foreach ($_GET as $key => $value) {
				if (!property_exists($className, $key) || !$value) {
					continue;
				}

				$filter->$key = $value;
			}
			$param["filter"] = $filter;

			$pager = \Modules\Base\Models\PagerViewModel::Instance($className::count(), isset($_GET["page"]) ? $_GET["page"] : 1);
			$param["pager"] = $pager;
			$param["models"] = method_exists($className, 'adminLoadCollection')
									? $className::adminLoadCollection((array) $filter, null, null, $pager->limit, $pager->offset)
									: $className::loadCollection((array) $filter, null, null, $pager->limit, $pager->offset);
		}

		/** @var \Modules\Base\Models\NestedMenu $menuModel*/
		$menuModel = Config::getInstance()->admin['menu']['model'];

		self::view($single
				? $fullMetadata->class->adminDetailsView ?: "Admin/edit"
				: $fullMetadata->class->adminListView ?: "Admin/list",
			array_merge($param, array(
				'siteName' => Config::getInstance()->siteName,
				'backend' => true,
				'menu' => $menuModel::getSiteMenu('admin', array('isSystem' => 1))
			)));
	}

	public function post($model, $id = 0) {
		if (!$model) {
			throw new HttpException(404, "Не задан обязательный параметр: model");
		}

		$className = Config::getInstance()->mvc['availableModels'][$model];

		if (!class_exists($className)) {
			throw new \Exception(sprintf("Модель %s не найдена", $className));
		}

		/* @var BaseModel $instance */
		$instance = new $className();
		$fields = (strpos(getenv('HTTP_CONTENT_TYPE') ?: getenv('CONTENT_TYPE'), 'application/json') === 0)
			? json_decode(file_get_contents("php://input"), true)
			: $_POST;

		$instance->map($fields, $_FILES);

		try {
			$instance->validate();
			$instance->save();

			if (strpos(getenv('HTTP_ACCEPT'), 'application/json') === 0) {
				self::json($instance);
				return;
			}

			self::redirect(isset($_REQUEST["redirectUrl"]) && $_REQUEST["redirectUrl"] == "!!self" ?
				"/admin/$model/{$instance->id}" :
				$_REQUEST["redirectUrl"] ?: "/admin/$model");
		}
		catch (ValidateException $e) {
			$this->get($model, $id == 0 ? "new" : $id, $e->fields);
		}
	}

	public function delete($model, $id = 0) {
		if (!$model || !$id || !is_numeric($id)) {
			throw new HttpException(404, "Не заданы обязательные параметры");
		}

		$className = Config::getInstance()->mvc['availableModels'][$model];

		if (!class_exists($className)) {
			throw new \Exception(sprintf("Модель %s не найдена", $className));
		}

		/* @var BaseModel $instance */
		$instance = new $className();
		$instance->{$className::$__primaryKey} = $id;

		$instance->remove();

		self::redirect($_GET["redirectUrl"] ?: "/admin/$model");
	}

	public function dashboard() {
		/** @var \Modules\Base\Models\NestedMenu $menuModel*/
		$menuModel = Config::getInstance()->admin['menu']['model'];

		self::view("Admin/admin",
			array(
				'siteName' => Config::getInstance()->siteName,
				'pageTitle' => sprintf(i18n::current()->f_siteAdministration, Config::getInstance()->siteName),
				'backend' => true,
				'menu' => $menuModel::getSiteMenu('admin', array('isSystem' => 1))
			));
	}

	public function onActionExecuting() {
		if (!\Modules\Membership\Models\User::getInstance()->isSystem) {
			self::redirect(sprintf("/Login?redirectUrl=%s", urlencode(\Modules\Framework\Utils\Url::getInstance()->getUrlCurrent())));
		}
	}
}

class Filter {
	public function __call($method, $args) {
		if (isset($this->$method)) {
			$func = $this->$method;
			return call_user_func_array($func, $args);
		}
	}
}

?>