<?php

namespace Modules\Admin\Controllers;

use Modules\Framework\Mvc\Model\BaseModel;
use Modules\Framework\Core\Config\Config;
use Modules\Framework\Validation\ValidateException;

class FileUpload extends \Modules\Framework\Mvc\Controller\Controller {
	static $upload_param_name = 'files';

	public function head($model, $parentModel, $parentId) {
		$this->get($model, $parentModel, $parentId);
	}

	public function get($model, $parentModel = '', $parentId = 0) {
		header('Content-type: application/json');

		if (!$model || !$parentId) {
			self::json(array('error' => 'Заданы неверные параметры'));
		}

		/* @var $className BaseModel */
		$className = Config::getInstance()->mvc["availableModels"][$model];

		$images = $className::LoadCollection(array("type" => $parentModel, $className::$__foreignKey => (int)$parentId));

		self::json(array_map(function ($image) use ($model, $parentModel, $parentId) {
				/* @var $image \Modules\Base\Models\PhotoGalleryImage */
				return $image->getFileObject();
			}, $images));
	}

	public function post($model, $parentModel = '', $parentId = 0, $id = 0) {
		if (!$model || !$parentId) {
			self::json(array('error' => 'Заданы неверные параметры'));
		}

		if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
			$this->delete($model, $parentId, $id);
		}

		$className = Config::getInstance()->mvc["availableModels"][$model];

		if (!class_exists($className)) {
			throw new \Exception(sprintf("Модель %s не найдена", $className));
		}

		$result = array();

		$_upload = $_FILES[self::$upload_param_name];

		if ($_upload && is_array($_upload['tmp_name'])) {
			foreach ($_upload['tmp_name'] as $index => $dummy) {
				$result[] =self::upload($_upload['tmp_name'][$index],
										$_upload['name'][$index],
										$_upload['size'][$index],
										$_upload['type'][$index],
										$_upload['error'][$index],
										function ($filename) use ($className, $model, $parentModel, $parentId, $index) {
											return FileUpload::uploadCallback($filename, $className, $model, $parentModel, $parentId, $index);
										});
			}
		} else {
			$result[] =self::upload($_upload['tmp_name'],
									$_upload['name'],
									$_upload['size'],
									$_upload['type'],
									$_upload['error'],
									function ($filename) use ($className, $model, $parentModel, $parentId) {
										return FileUpload::uploadCallback($filename, $className, $model, $parentModel, $parentId, -1);
									});
		}

		self::json($result);
	}

	public function delete($model, $parentModel = '', $parentId = 0, $id = 0) {
		if (!$model || !$parentId || !$id) {
			self::json(array('error' => 'Заданы неверные параметры'));
		}

		$className = Config::getInstance()->mvc["availableModels"][$model];

		if (!class_exists($className)) {
			throw new \Exception(sprintf("Модель %s не найдена", $className));
		}

		/* @var $instance BaseModel */
		$instance = new $className($id);
		$instance->Remove();

		if (isset($_REQUEST['file']) && $_REQUEST['file']) {
			$file_name = basename(stripslashes($_REQUEST['file']));

			$file_path = Config::getInstance()->fileUploadDir . $file_name;

			$success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);
		} else {
			$success = true;
		}

		self::json($success);
	}

	public function onActionExecuting() {
		if (!\Modules\Membership\Models\User::getInstance()->isSystem) {
			self::redirect(sprintf("/login?redirecturl=%s", urlencode(\Modules\Framework\Utils\Url::getInstance()->getUrlCurrent())));
		}
	}

	static function upload($_tmp_name, $_name, $_size, $_type, $_error, $onsuccess) {
		if ($_error != 0) {
			return array('error' => $_error);
		}

		if (!$_size) {
			return array('error' => 'acceptFileTypes');
		}

		$extension = strtolower(pathinfo($_name, \PATHINFO_EXTENSION));

		if (!in_array($extension, array("gif", "jpg", "png", "jpeg"))) {
			return array('error' => 'acceptFileTypes');
		}

		$filepath = Config::getInstance()->fileUploadDir . $_name;
		$filename = strtolower(pathinfo($_name, \PATHINFO_FILENAME));

		if (file_exists($filepath)) {
			$i = 1;
			while (file_exists(Config::getInstance()->fileUploadDir . "$filename($i).$extension")) $i++;
			$filepath = Config::getInstance()->fileUploadDir . "$filename($i).$extension";
		}

		if (move_uploaded_file($_tmp_name, $filepath)) {
			chmod($filepath, 0777);
			$filename = pathinfo($filepath, \PATHINFO_BASENAME);

			return $onsuccess($filename);

		} else {
			return array('error' => 'Ошибка загрузки файла');
		}
	}

	static function uploadCallback($filename, $className, $model, $parentModel, $parentId, $post_index) {
		try {
			/* @var $instance \Modules\Base\Models\PhotoGalleryImage */
			$instance = new $className();
			$instance->mapUploadParams($filename, $post_index);
			$instance->validate();
			$instance->save();

			return $instance->getFileObject();
		}
		catch (ValidateException $e) {
			return array('error' => $e->fields);
		}
		catch (\Exception $e) {
			return array('error' => 'Some error occurred');
		}
	}
}

?>