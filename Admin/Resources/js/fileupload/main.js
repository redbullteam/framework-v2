/*
 * jQuery File Upload Plugin JS Example 6.7
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function () {
    'use strict';

    $('.fileupload')
		.each(function () {
            var $this = $(this),
				action = $this.data('controller');
			
			$this.fileupload({
					url: action
				})
				.bind('fileuploadsubmit', function (e, data) {
					var inputs = data.context.find(':input');

					if (inputs.filter('[required][value=""]').first().focus().length) {
						data.context.find('button').prop('disabled', false);
						return false;
					}

					data.formData = inputs.serializeArray();
				});
			
            $.getJSON(action, function (result) {
                if (result && result.length) {
                    $this.fileupload('option', 'done')
                        .call($this[0], null, {result: result});
                }
            });
        });
});
