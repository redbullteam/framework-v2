/*
 * jQuery File Upload Plugin Localization Example 6.5.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*global window */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "Файл слишком большо",
            "minFileSize": "Файл слишком маленький",
            "acceptFileTypes": "Недопустимый тип файла",
            "maxNumberOfFiles": "Превышено максимальное количество файлов",
            "uploadedBytes": "Превышен лимит памяти",
            "emptyResult": "Неверный ответ от сервера"
        },
        "error": "Ошибка",
        "start": "Начать",
        "cancel": "Отмена",
        "destroy": "Удалить"
    }
};
