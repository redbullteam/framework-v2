<?php
/**
 * @var \Modules\Framework\Utils\Url $__url
 * @var \Modules\Base\Models\Menu[] $menu
 */
$url = sprintf('/admin/%s', isset($__url->params['model']) ? $__url->params['model'] : '');
?><?php foreach ($menu as $menuItem): ?>
<li <?php if ($menuItem->selected($url)): ?>class="active" <?php endif; ?>><a href="<?=$menuItem->url?>"><?=$menuItem->title?></a></li>
<?php endforeach; ?>