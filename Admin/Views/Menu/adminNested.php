<?php
/**
 * @var \Modules\Framework\Utils\Url $__url
 * @var \Modules\Base\Models\NestedMenu[] $menu
 * @var \Modules\Base\Models\NestedMenu $menuItem
 * @var \Modules\Base\Models\NestedMenu $subItem
 */
$menu = \Modules\Base\Models\NestedMenu::group($menu, "parentId");
$url = sprintf('/admin/%s', isset($__url->params['model']) ? $__url->params['model'] : '');
?>
<?php foreach (reset($menu["parentId"]) as $menuItem): ?>
	<?php if (!empty($menu["parentId"][$menuItem->id])): ?>
		<li id="<?=$menuItem->id?>" class="dropdown<?php if ($menuItem->selected($url)): ?> active<?php endif; ?>">
			<a href="<?=$menuItem->url?>" class="dropdown-toggle" data-target="#<?=$menuItem->id?>" data-toggle="dropdown">
				<?=$menuItem->title?>
				<b class="caret"></b>
			</a>
			<ul class="dropdown-menu">
				<?php foreach ($menu["parentId"][$menuItem->id] as $subItem): ?>
				<li <?php if ($subItem->selected()): ?>class="active" <?php endif; ?>><a href="<?=$subItem->url?>"><?=$subItem->title?></a></li>
				<?php endforeach; ?>
			</ul>
		</li>
	<?php else: ?>
		<li <?php if ($menuItem->selected($url)): ?>class="active" <?php endif; ?>><a href="<?=$menuItem->url?>"><?=$menuItem->title?></a></li>
	<?php endif; ?>
<?php endforeach; ?>