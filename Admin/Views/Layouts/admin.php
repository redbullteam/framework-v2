<?php
/* @var \Modules\Framework\Mvc\View\PhpView $this */
?><!DOCTYPE html>
<html lang="en" ng-app="admin">
<head>
    <meta charset="utf-8">
    <title><?=$this->pageTitle?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="/Resources/Admin/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body { padding: 60px 0 40px; }
    </style>
    <link href="/Resources/Admin/css/bootstrap-responsive.min.css" rel="stylesheet">
	<?=$this->stylesheet?>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script>window.html5 || document.write('<script src="/Resources/Base/js/html5shiv.js"><\/script>')</script>
    <![endif]-->
</head>

<body style="background: none;">

<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/admin/"><?=\Modules\Framework\Globalization\i18n::current()->siteAdministration;?></a>
            <div class="nav-collapse">
                <ul class="nav">
					<?=$this->partial(\Modules\Framework\Core\Config\Config::getInstance()->admin['menu']['view'])?>
                </ul>
                <ul class="nav pull-right">
                    <li><a href="/"><?=\Modules\Framework\Globalization\i18n::current()->goToSite;?></a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>

<div class="container">
	<?=$this->body?>

    <footer>
        <p>&copy; <?=$this->siteName?> <?=date('Y')?></p>
    </footer>
</div> <!-- /container -->

<script src="//yandex.st/jquery/1.8.0/jquery.min.js"></script>
<script>!window.jQuery && document.write(unescape('%3Cscript src="/Resources/Base/js/jquery.min.js"%3E%3C/script%3E'))</script>
<script src="/Resources/Admin/js/bootstrap.min.js"></script>

<?=$this->javascript?>

</body>
</html>
