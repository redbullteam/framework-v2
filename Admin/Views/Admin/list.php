<?php
use Modules\Framework\Globalization\i18n;

/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var \Modules\Framework\Utils\Url $__url
 * @var \Modules\Framework\Core\Metadata\DynamicMetadata $fields
 * @var \Modules\Framework\Mvc\Model\BaseModel $models
 */
$this->extend(\Modules\Framework\Core\Config\Config::getInstance()->admin['layout']);
?>
<?php $__section->begin('body'); ?>
<h2><?=$pageTitle?></h2>
<?php if (array_filter((array) $fields, function ($field) { return $field->filterable; })): ?>
	<form action="" class="form-inline" method="get" enctype="multipart/form-data">
		<?php foreach ($fields as $fieldName => $field): ?>
			<?php if (!$field->filterable) continue; ?>
			<label class="control-label" for="<?=$fieldName?>"><?=$field->display?>:</label>
			<?=$this->partial("Base:Shared/Form/Fields/" . ($field->uiHint ?: ($field->validation && $field->validation->datatype ? $field->validation->datatype : "text")), array('field' => $field, 'disableRequired' => true, 'model' => $filter))?>
		<?php endforeach; ?>
		<button type="submit" class="btn"><i class="icon-filter"></i> <?=i18n::current()->apply;?></button>
	</form>
<?php endif; ?>
<?php if ($models): ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<?php foreach ($fields as $fieldName => $field): ?>
					<?php if ($field->hideFromList || ($field->debug == "true" && !$debugEnabled)) continue; ?>
					<th><?=$field->display?></th>
				<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($models as $model): ?>
				<tr onclick="document.location.href = '<?=$__url->getUrlCurrent()?>/<?=$model->id?>?redirectUrl=<?=urlencode($__url->getRawUrl())?>';">
					<?php foreach ($fields as $fieldName => $field): ?>
						<?php if ($field->hideFromList || ($field->debug == "true" && !$debugEnabled)) { continue; } ?>
						<?php if ($field->type == "file" && $model->$fieldName): ?>
							<td style="text-align: center;"><a href="/upload/<?=$model->$fieldName?>"><img src="/upload/90x36/<?=$model->$fieldName?>" alt="Изображение"></a></td>
						<?php else: ?>
							<?php
								if ($field->adminListView) {
									$methodName = (string) $field->adminListView;
									$str = $model->$methodName();
								} else {
									$str =  strip_tags($model->$fieldName);
									$str = (mb_strlen($str, 'utf-8') > 100) ? mb_substr($str, 0, 100, 'utf-8') . '...' : $str;
								}
							?>
							<td><?=$str?></td>
						<?php endif; ?>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?=$this->partial("Base:Shared/pager", array("pager" => $pager))?>
<?php else: ?>
<div class="alert alert-error">
	<?=i18n::current()->listIsEmpty;?>
	<?php
		$filterValues = (array) $filter;
		if (!empty($filterValues))
			echo i18n::current()->resetFilterValues;
	?>
</div>
<?php endif; ?>

<?php if ($debugEnabled || !$modelPersist): ?>
<div class="form-actions">
	<a href="<?=$__url->getUrlCurrent()?>/new?redirectUrl=<?=urlencode($__url->getRawUrl())?>" class="btn btn-primary"><i class="icon-plus icon-white"></i> <?=i18n::current()->add;?></a>
</div>
<?php endif; ?>
<?php $__section->end(); ?>