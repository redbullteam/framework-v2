<?php
/* @var \Modules\Framework\Mvc\View\PhpView $this */
/* @var \Modules\Framework\Mvc\View\Helpers\Section $__section */
?>
<?php if (!$new): ?>
<?php $__section->append('javascript'); ?>
	<script src="/Resources/Admin/js/fileupload/vendor/jquery.ui.widget.js"></script>
	<script src="/Resources/Admin/js/fileupload/vendor/tmpl.min.js"></script>
	<script src="/Resources/Admin/js/fileupload/load-image.min.js"></script>
	<script src="/Resources/Admin/js/fileupload/canvas-to-blob.min.js"></script>
	<script src="/Resources/Admin/js/fileupload/bootstrap-image-gallery.min.js"></script>
	<script src="/Resources/Admin/js/fileupload/jquery.iframe-transport.js"></script>
	<script src="/Resources/Admin/js/fileupload/jquery.fileupload.js"></script>
	<script src="/Resources/Admin/js/fileupload/jquery.fileupload-fp.js"></script>
	<script src="/Resources/Admin/js/fileupload/jquery.fileupload-ui.js"></script>
	<script src="/Resources/Admin/js/fileupload/locale.js"></script>
	<script src="/Resources/Admin/js/fileupload/main.js"></script>
<?php $__section->end(); ?>
<?php $__section->append('stylesheet'); ?>
	<link rel="stylesheet" href="/Resources/Admin/css/bootstrap-image-gallery.min.css"/>
	<link rel="stylesheet" href="/Resources/Admin/css/jquery.fileupload-ui.css"/>
<?php $__section->end(); ?>
<?php endif; ?>
<fieldset class="fileupload" data-controller="<?=$controller?>">
	<legend>
		Фотографии
	</legend>
	<?php if ($new): ?>
	<div class="alert alert-error">
		Фотографии нельзя задавать в новой галерее. Сохраните изменения и попробуйте снова.
	</div>
	<?php else: ?>
	<div class="row fileupload-buttonbar">
		<div class="span7">
				<span class="btn btn-success fileinput-button">
					<i class="icon-plus icon-white"></i>
					<span>Добавить</span>
					<input type="file" name="files[]" multiple>
				</span>
			<button type="submit" class="btn btn-primary start">
				<i class="icon-upload icon-white"></i>
				<span>Загрузить все</span>
			</button>
			<button type="reset" class="btn btn-warning cancel">
				<i class="icon-ban-circle icon-white"></i>
				<span>Отменить</span>
			</button>
			<button type="button" class="btn btn-danger delete">
				<i class="icon-trash icon-white"></i>
				<span>Удалить</span>
			</button>
			<input type="checkbox" class="toggle">
		</div>
		<div class="span5 fileupload-progress fade">
			<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				<div class="bar" style="width: 0;"></div>
			</div>
			<div class="progress-extended">&nbsp;</div>
		</div>
	</div>
	<div class="fileupload-loading"></div>
	<br>
	<table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
	<?php endif;?>
</fieldset>
<?php if (!$new): ?>
<script id="template-upload" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
	<tr class="template-upload fade">
		<td class="preview"><span class="fade"></span></td>
		<td class="title"><label>Title: <input type="text" name="title[]" value="{%=(file.title || file.name)%}" required></label></td>
		<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
		{% if (file.error) { %}
		<td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
		{% } else if (o.files.valid && !i) { %}
		<td>
			<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
		</td>
		<td class="start">{% if (!o.options.autoUpload) { %}
			<button class="btn btn-primary">
				<i class="icon-upload icon-white"></i>
				<span>{%=locale.fileupload.start%}</span>
			</button>
			{% } %}</td>
		{% } else { %}
		<td colspan="2"></td>
		{% } %}
		<td class="cancel">{% if (!i) { %}
			<button class="btn btn-warning">
				<i class="icon-ban-circle icon-white"></i>
				<span>{%=locale.fileupload.cancel%}</span>
			</button>
			{% } %}</td>
	</tr>
	{% } %}
</script>
<script id="template-download" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
	<tr class="template-download fade">
		{% if (file.error) { %}
		<td></td>
		<td class="title">{%=(file.title || file.name)%}</td>
		<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
		<td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
		{% } else { %}
		<td class="preview">{% if (file.thumbnail_url) { %}
			<a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
			{% } %}</td>
		<td class="title"><label>Title: <input type="text" name="title[]" value="{%=(file.title || file.name)%}" required></label></td>
		<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
		<td colspan="2"></td>
		{% } %}
		<td class="delete">
			<button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
				<i class="icon-trash icon-white"></i>
				<span>{%=locale.fileupload.destroy%}</span>
			</button>
			<input type="checkbox" name="delete" value="1">
		</td>
	</tr>
	{% } %}
</script>
<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd">
	<div class="modal-header">
		<a class="close" data-dismiss="modal">&times;</a>
		<h3 class="modal-title"></h3>
	</div>
	<div class="modal-body"><div class="modal-image"></div></div>
	<div class="modal-footer">
		<a class="btn modal-download" target="_blank">
			<i class="icon-download"></i>
			<span>Download</span>
		</a>
		<a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
			<i class="icon-play icon-white"></i>
			<span>Slideshow</span>
		</a>
		<a class="btn btn-info modal-prev">
			<i class="icon-arrow-left icon-white"></i>
			<span>Previous</span>
		</a>
		<a class="btn btn-primary modal-next">
			<span>Next</span>
			<i class="icon-arrow-right icon-white"></i>
		</a>
	</div>
</div>
<?php endif; ?>