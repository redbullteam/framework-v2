<?php
/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Utils\Url $__url
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 */
$this->extend(\Modules\Framework\Core\Config\Config::getInstance()->admin['layout']);
?>
<?php $__section->begin('body'); ?>
<h2><?=$pageTitle?></h2>
<?php if ($models): ?>
	<table class="table tree-view">
		<thead>
			<tr>
				<?php foreach ($fields as $fieldName => $field): ?>
					<?php if ($field->hideFromList) continue; ?>
					<th><?=$field->display?></th>
				<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($models as $model): ?>
				<tr id="<?=$model->id?>" data-parent="<?=$model->parentId?>" data-level="<?=$model->getNestedLevel()?>">
					<?php $firstColumn = true; ?>
					<?php foreach ($fields as $fieldName => $field): ?>
						<?php if ($field->hideFromList) continue; ?>
						<td <?php if ($firstColumn):?> style="padding-left: <?=($model->getNestedLevel() - 1) * 30 + 8?>px;"<?php endif;?>>
							<?php $str = strip_tags($model->$fieldName); ?>
                    		<?=(mb_strlen($str, 'utf-8') > 100) ? mb_substr($str, 0, 100, 'utf-8') . '...' : $str;?>
						</td>
						<?php $firstColumn = false; ?>
					<?php endforeach; ?>
					<td><a href="<?=$__url->getUrlCurrent()?>/<?=$model->id?>"><i class="icon-pencil"></i></a></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
<div class="alert alert-error">
	Список пуст
</div>
<?php endif; ?>

<div class="form-actions">
	<a href="<?=$__url->getUrlCurrent()?>/new" class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить</a>
</div>
<?php $__section->end(); ?>

<?php $__section->append('javascript'); ?>
<script>
	$(function () {
		var openedNodes = [],
			localStorageKey = 'openedNodes<?=$model->id?>';

        if (window.localStorage) {
            var openedNodes = window.localStorage.getItem(localStorageKey);
			if (openedNodes) {
                openedNodes = JSON.parse(openedNodes);
			}
        }

        if (!openedNodes) {
            openedNodes = $('.tree-view').find('tr[data-level="1"]').map(function (index, item) {
                return item.id;
            }).get();

            window.localStorage.setItem(localStorageKey, JSON.stringify(openedNodes));
        }

		$('.tree-view')
			.find('tbody tr').not('[data-parent="0"]').hide().end()
			.find('td:first-child').each(function () {
				var tr = $(this).parent(),
					id = tr.attr('id'),
					hasChildren = tr.next().data('parent') == id,
					opened = $.inArray(id, openedNodes) > -1,
					icon = $('<i>', {
							"class": hasChildren ?
                                (opened ? "icon-minus-sign" : "icon-plus-sign") :
								"icon-remove-circle",
							css: { marginRight: 7, cursor: hasChildren ? "pointer" : "default" }
						})
						.prependTo(this);

				if (hasChildren) {
					icon.click(function(e) {
						var self = $(this);
						if (self.hasClass('icon-plus-sign')) {
							self.removeClass('icon-plus-sign').addClass('icon-minus-sign');
							tr.siblings('[data-parent="' + id + '"]')
								.find('td').wrapInner('<div style="display: none;">').end()
								.show()
								.find('div').slideDown(300, function () {
									var set = $(this);
									set.replaceWith(set.contents());
								});

							if (window.localStorage) {
								var openedNodes = window.localStorage.getItem(localStorageKey);
								if (openedNodes) {
                                    openedNodes = JSON.parse(openedNodes);
								} else {
                                    openedNodes =  [];
								}
								if ($.inArray(id, openedNodes) == -1) {
                                    openedNodes.push(id);
								}

                                window.localStorage.setItem(localStorageKey, JSON.stringify(openedNodes));
							}
						} else {
							self.removeClass('icon-minus-sign').addClass('icon-plus-sign');
							tr.siblings('[data-parent="' + id + '"]')
								.find('td').wrapInner('<div style="display: block;">').end()
								.find('div').slideUp(300, function () {
									$(this).parent().parent().hide();
									var set = $(this);
									set.replaceWith(set.contents());
								});

                            if (window.localStorage) {
                                var openedNodes = window.localStorage.getItem(localStorageKey);
                                if (openedNodes) {
                                    openedNodes = JSON.parse(openedNodes);

                                    var pos = $.inArray(id, openedNodes);
                                    if (pos > -1) {
                                       	openedNodes.splice(pos, 1);
                                        window.localStorage.setItem(localStorageKey, JSON.stringify(openedNodes));
                                    }
                                }
                            }
						}

						return false;
					});
				}

				if (opened) {
                    tr.siblings('[data-parent="' + id + '"]').show();
				}
			});
	});
</script>
<?php $__section->end(); ?>