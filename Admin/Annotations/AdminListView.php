<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY"})
 */
class AdminListView implements \Modules\Framework\Core\Metadata\IMetadata {
	public $text;

	public function __construct(array $values) {
		$this->text = $values['value'];
	}

	public function __toString() {
		return $this->text;
	}

	public function getMetadataKey() {
		return "adminListView";
	}
}
