<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target("CLASS")
 */
class Persist implements \Modules\Framework\Core\Metadata\IMetadata {
	public $value = false;

	public function getMetadataKey() {
		return "persist";
	}
}
