<?php

namespace Modules\Admin\Annotations;

use Modules\Framework\Globalization\i18n;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY"})
 */
class Display implements \Modules\Framework\Core\Metadata\IMetadata {
	public $text;

	public function __construct(array $values) {
		$this->text = $values['value'];
	}

	public function __toString() {
		return !strncmp($this->text, 'i18n::', 6)
			? i18n::current()->{substr($this->text, 6)} ?: substr($this->text, 6)
			: $this->text;
	}

	public function getMetadataKey() {
		return "display";
	}
}
