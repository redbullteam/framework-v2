<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Enum implements \Modules\Framework\Core\Metadata\IMetadata {
	public $values;

	public function __construct(array $values) {
		$this->values = $values['value'];
	}

	public function __toString() {
		return $this->values;
	}

	public function getMetadataKey() {
		return "enum";
	}
}
