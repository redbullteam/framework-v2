<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class HideFromDetails implements \Modules\Framework\Core\Metadata\IMetadata {
	public $value = false;

	public function getMetadataKey() {
		return "hideFromDetails";
	}
}
