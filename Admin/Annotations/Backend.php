<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Backend implements \Modules\Framework\Core\Metadata\IMetadata {
	public function getMetadataKey() {
		return "backend";
	}
}
