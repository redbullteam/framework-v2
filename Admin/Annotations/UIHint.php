<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY"})
 */
class UIHint implements \Modules\Framework\Core\Metadata\IMetadata {
	public $text;
	public $sql;
	public $enum;

	public function __construct(array $values) {
		$this->text = isset($values['value']) ? $values['value'] : '';
		$this->sql = isset($values['sql']) ? $values['sql'] : null;
		$this->enum = isset($values['enum']) ? $values['enum'] : null;
	}

	public function __toString() {
		return $this->text;
	}

	public function getMetadataKey() {
		return "uiHint";
	}
}
