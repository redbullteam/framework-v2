<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Readonly implements \Modules\Framework\Core\Metadata\IMetadata {
	public $value = false;

	public function getMetadataKey() {
		return "readonly";
	}
}
