<?php

namespace Modules\Admin\Annotations;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Debug implements \Modules\Framework\Core\Metadata\IMetadata {
	public function getMetadataKey() {
		return "debug";
	}
}
