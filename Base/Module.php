<?php

namespace Modules\Base;

use Modules\Framework\Core\Config\Config;

class Module extends \Modules\Framework\Core\ModuleSystem\Module {
	public $dependencies = array("Framework", "Admin");
	public $autoInstall = true;

	public function install() {
		// Menu
		// hardcode -_-
		if (Config::getInstance()->admin['menu']['model'] == '\Modules\Base\Models\NestedMenu') {
			\Modules\Framework\Core\DB::Command()->query(
				sprintf(
					"CREATE TABLE IF NOT EXISTS `%sMenu` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`parentId` int(11) NOT NULL,
					`nestedLevel` varchar(255) NOT NULL,
					`name` varchar(255) NOT NULL,
					`title` varchar(255) NOT NULL,
					`url` varchar(255) NOT NULL,
					`image` varchar(255) NOT NULL,
					`sortOrder` int(11) NOT NULL,
					`isSystem` int(1) NOT NULL,
					PRIMARY KEY (`id`),
					UNIQUE KEY `name` (`name`),
					KEY `nestedLevel` (`nestedLevel`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1",
					Config::getInstance()->connection['dbPrefix']));

			$db = \Modules\Framework\Core\DB::Command()->prepare(
				sprintf(
					"INSERT INTO `%sMenu` (id, parentId, nestedLevel, name, title, url, isSystem)
					 VALUES (:id, :parentId, :nestedLevel, :name, :title, :url, :isSystem)
					 ON DUPLICATE KEY UPDATE url = url",
						Config::getInstance()->connection['dbPrefix']));

			$db->execute(array(
				'id' => 1,
				'parentId' => 0,
				'nestedLevel' => '1',
				'name' => 'admin',
				'title' => 'admin',
				'url' => '/admin',
				'isSystem' => true
			));

			$db->execute(array(
				'id' => 2,
				'parentId' => 0,
				'nestedLevel' => '2',
				'name' => 'top',
				'title' => 'top',
				'url' => '',
				'isSystem' => false
			));

			$db->execute(array(
				'id' => 3,
				'parentId' => 1,
				'nestedLevel' => '1.1',
				'name' => '',
				'title' => 'Меню',
				'url' => '/admin/NestedMenu',
				'isSystem' => true
			));
		} else {
			\Modules\Framework\Core\DB::Command()->query(
				sprintf(
					"CREATE TABLE IF NOT EXISTS `%sMenu` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`title` varchar(255) NOT NULL,
					`url` varchar(255) NOT NULL,
					`image` varchar(255) NOT NULL,
					`sortOrder` int(11) NOT NULL,
					`isSystem` int(1) NOT NULL,
					PRIMARY KEY (`id`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1",
					Config::getInstance()->connection['dbPrefix']));

			$db = \Modules\Framework\Core\DB::Command()->prepare(
				sprintf(
					"INSERT INTO `%sMenu` (id, title, url, isSystem)
					 VALUES (:id, :title, :url, :isSystem)
					 ON DUPLICATE KEY UPDATE url = VALUES(url)",
						Config::getInstance()->connection['dbPrefix']));

			$db->execute(array(
				'id' => 1,
				'title' => 'Меню',
				'url' => '/admin/Menu',
				'isSystem' => true
			));
		}

		// Article
		\Modules\Framework\Core\DB::Command()->query(
			sprintf(
				"CREATE TABLE IF NOT EXISTS `%sArticle` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`alias` varchar(255) NOT NULL,
					`image` varchar(255) NOT NULL,
					`title` varchar(255) NOT NULL,
					`text` text NOT NULL,
					`metaKeywords` varchar(255) NOT NULL,
					`metaDescription` text NOT NULL,
					PRIMARY KEY (`id`),
					UNIQUE KEY `alias` (`alias`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1", 
					Config::getInstance()->connection['dbPrefix']));
			
		parent::install();
	}
}