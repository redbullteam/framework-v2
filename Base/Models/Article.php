<?php

namespace Modules\Base\Models;

use Modules\Framework\Utils\TextUtils;

/**
 * @Display("i18n::articles")
 */
class Article extends \Modules\Framework\Mvc\Model\BaseModel {
	/**
	 * @Display("i18n::title")
	 * @Required
	 */
	public $title;

	/**
	 * @Display("i18n::metaKeywords")
	 * @HideFromList
	 */
	public $metaKeywords;

	/**
	 * @Display("i18n::metaDescription")
	 * @DataType(DataType::MULTILINETEXT)
	 * @HideFromList
	 */
	public $metaDescription;

	/**
	 * @Display("i18n::text")
	 * @Required
	 * @DataType(DataType::HTML)
	 */
	public $text;

	/**
	 * @Display("i18n::alias")
	 * @Unique
	 */
	public $alias;

	protected function endMapping() {
		if (empty($this->alias)) {
			$this->alias = TextUtils::makeAlias($this->title);
		}
	}
}