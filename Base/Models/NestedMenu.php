<?php

namespace Modules\Base\Models;

use Modules\Framework\Core\Application;
use Modules\Framework\Core\Config\Config;
use Modules\Framework\Cache\Cache;

/**
 * @Display("i18n::menu")
 * @TableName("Menu")
 * @AdminListView("Admin/tree")
 */
class NestedMenu extends \Modules\Framework\Mvc\Model\TreeModel {
	/**
	 * @Display("i18n::title")
	 * @Required
	 */
	public $title;

	/**
	 * @Display("i18n::name")
	 * @Unique
	 */
	public $name;

	/**
	 * @Display("i18n::url")
	 * @Required
	 */
	public $url;

	/**
	 * @Display("i18n::isSystem")
	 * @DataType(DataType::BOOLEAN)
	 * @Debug
	 */
	public $isSystem = false;

	public function selected($url = null) {
		return trim($url ?: \Modules\Framework\Utils\Url::getInstance()->getUrlPath(), '/') == trim($this->url, '/');
	}

	public static function getSiteMenuCached($name = 'top', $params = array("isSystem" => 0), $lifeTime = 0) {
		return \Modules\Framework\Cache\Cache::getInstance()->getOrSet(__CLASS__ . '/' . $name,
			function () use ($name, $params) {
				return NestedMenu::getSiteMenu($name, $params);
			},
			$lifeTime);
	}

	public static function getSiteMenu($name = 'top', $params = array("isSystem" => 0)) {
		$bindParams = self::modifySqlParams($params, 't1.');

		$bindParams['name'] = $name;

		$where = "t1.nestedLevel LIKE CONCAT(t2.nestedLevel, '.%%') AND t2.name = :name ";

		$where = !empty($params) ?
			sprintf('WHERE %s', implode(" AND ", $params))  . ' AND ' . $where:
			'WHERE ' . $where;

		$sql = sprintf("SELECT t1.*
						FROM `%1\$s%2\$s` t1, `%1\$s%2\$s`t2
						%3\$s
						ORDER BY t1.nestedLevel",
							Config::getInstance()->connection['dbPrefix'],
							self::getName(),
							$where);

		$db = \Modules\Framework\Core\DB::Command()->prepare($sql);

		$db->execute($bindParams);

		$result = $db->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, get_called_class());

		/* @var NestedMenu $model */
		foreach ($result as $model) {
			$model->afterLoad();
		}

		return $result;
	}

	public static function adminLoadCollection($params = null, $columns = null) {
		if (!Application::getInstance()->isDebugMode())
			$params["isSystem"] = false;

		return self::LoadCollection($params, $columns, "nestedLevel");
	}

	protected function afterSave() {
		Cache::getInstance()->flushAll(__CLASS__);

		parent::afterSave();
	}
}