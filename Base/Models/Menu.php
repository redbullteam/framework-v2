<?php

namespace Modules\Base\Models;

use Modules\Framework\Core\Application;

/**
 * @Display("i18n::menu")
 */
class Menu extends \Modules\Framework\Mvc\Model\BaseModel {
	/**
	 * @Display("i18n::title")
	 * @Required
	 */
	public $title;

	/**
	 * @Display("i18n::url")
	 * @Required
	 */
	public $url;

	/**
	 * @Display("i18n::sortOrder")
	 */
	public $sortOrder = 100;

	/**
	 * @Display("i18n::isSystem")
	 * @DataType(DataType::BOOLEAN)
	 * @Debug
	 */
	public $isSystem = false;

	public function selected($url = null) {
		$s = \Modules\Framework\Utils\Url::getInstance()->getUrlPath();
		return trim($url ?: $s, '/') == trim($this->url, '/');
	}

	public static function getSiteMenu($name = 'top', $params = array("isSystem" => 0)) {
		return self::loadCollection($params, null, "sortOrder");
	}

	public static function getSiteMenuCached($name = 'top', $params = array("isSystem" => 0), $lifeTime = 0) {
		return \Modules\Framework\Cache\Cache::getInstance()->getOrSet(__CLASS__ . '/' . $name,
				function () use ($name, $params) {
					return Menu::getSiteMenu($name, $params);
				},
				time() + 86400);
	}

	public static function adminLoadCollection($params = null, $columns = null) {
		return \Modules\Framework\Core\Application::getInstance()->isDebugMode() ?
			self::LoadCollection($params, $columns) :
			self::LoadCollection(array("isSystem" => false), $columns);
	}
}