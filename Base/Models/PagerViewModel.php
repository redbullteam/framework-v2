<?php

namespace Modules\Base\Models;

class PagerViewModel {
	public $selected;
	public $offset;
	public $totalItems;
	public $totalPages;
	public $pageSize;
	public $first;
	public $last;
	public $limit;
	public $linkTemplate;

	public function __construct($totalItems, $page = 1, $limit = 20, $pageSize = 5) {
		$this->selected = $page > 0 ? $page : 1;
		$this->limit = $limit > 0 ? $limit : 10;
		$this->offset = ($page - 1) * $limit;

		$this->pageSize = max($pageSize, 1);

		$this->totalItems = $totalItems;
		$this->totalPages = (int) ceil($totalItems / $limit);

		$this->first = max(min($this->selected - floor($this->pageSize / 2), $this->totalPages - $this->pageSize + 1), 1);
		$this->last = min($this->first + $this->pageSize - 1, $this->totalPages);

		$this->linkTemplate = '?page=%d';
	}

	public static function Instance($totalItems, $page = 1, $limit = 20, $pageSize = 5) {
		return new PagerViewModel($totalItems, $page, $limit, $pageSize);
	}

	public function getPages() {
		return range($this->first, $this->last);
	}

	public function setLinkTemplate($linkTemplate) {
		$this->linkTemplate = $linkTemplate;
		return $this;
	}

	public function link($page) {
		return sprintf($this->linkTemplate, $page);
	}
}