<?php

namespace Modules\Base\Controllers;

use Modules\Framework\Core\Config\Config;
use Modules\Framework\Mvc\Routing\HttpException;

class FileUtils {
	public function get($path, $filename, $width, $height, $crop) {
		$path = rtrim(Config::getInstance()->fileUploadDir, '/' . \DIRECTORY_SEPARATOR) . implode(\DIRECTORY_SEPARATOR, explode('/', $path)) . \DIRECTORY_SEPARATOR;
		
		$fullName = $path . $filename;

		if (!in_array(strtolower(pathinfo($fullName, \PATHINFO_EXTENSION)), array("gif","jpg", "png", "jpeg"))) {
			throw new HttpException(500, "Invalid file");
		}

		if (!file_exists($fullName)) {
			throw new HttpException(404, "File not found");
		}

		$mimetype = exif_imagetype($fullName);
		if ($mimetype == \IMAGETYPE_JPEG) {
			$type = 'jpg';
			$big = imagecreatefromjpeg($fullName);
		}

		if ($mimetype == \IMAGETYPE_GIF) {
			$type = 'gif';
			$big = @imagecreatefromgif($fullName);
		}

		if ($mimetype == \IMAGETYPE_PNG) {
			$type = 'png';
			$big = @imagecreatefrompng($fullName);
		}

		if (!isset($big)) {
			throw new HttpException(500, "Invalid file");
		}

		$w = imagesx($big);
		$h = imagesy($big);

		$newX = $width ?: $w;
		$newY = $height ?: $h;

		$doCrop = $crop === "crop/";

		if (($w > $newX) || ($h > $newY)) {
			if ($doCrop) {
				if ($w * $newY > $h * $newX) {
					$newW = round($newX * ($h / $newY));
					$srcX = round(($w - $newW) / 2);
					$srcY = 0;
					$w = round($newX * ($h / $newY));
				} else {
					$newH = round($newY * ($w / $newX));
					$srcX =  0;
					$srcY = round(($h - $newH) / 2);
					$h = $newH;
				}

				$small = imagecreatetruecolor($newX, $newY);
				imagecopyresampled($small, $big, 0, 0, $srcX, $srcY, $newX, $newY, $w, $h);
			} else {
				if (!$height) {
					$w_new = $newX;
					$h_new = round($h / ($w / $newX));
				} elseif (!$width) {
					$h_new = $newY;
					$w_new = round($w / ($h / $newY));
				} elseif ($w / $newX > $h / $newY){
					$w_new = $newX;
					$h_new = round($h / ($w / $newX));
				} else {
					$h_new = $newY;
					$w_new = round($w / ($h / $newY));
				}

				$small = imagecreatetruecolor($w_new, $h_new);
				imagecopyresampled($small, $big, 0, 0, 0, 0, $w_new, $h_new, $w, $h);
			}
		}
		else {
			$small = $big;
		}

		$fullName = $path . $width . 'x' . $height . \DIRECTORY_SEPARATOR . rtrim($crop, '/') . \DIRECTORY_SEPARATOR;

		if (!file_exists($fullName)) {
			mkdir($fullName, 0777, true);
		}

		$fullName .= $filename;

		switch($type) {
			case 'jpg':
				imagejpeg($small, $fullName, 85);
				Header("Content-type: image/jpeg");
				imagejpeg($small, '', 85);
				break;
			case 'gif':
				imagegif($small, $fullName);
				Header("Content-type: image/gif");
				imagegif($small);
				break;
			case 'png':
				imagepng($small, $fullName);
				Header("Content-type: image/png");
				imagepng($small, '');
				break;
		}

		@imagedestroy($big);
		@imagedestroy($small);
	}
}

?>