<?php

namespace Modules\Base\Controllers;

class Articles extends \Modules\Framework\Mvc\Controller\Controller {
	public function get($alias, $customView = 'article', $pageTitle = null) {
		$model = new \Modules\Base\Models\Article();

		$model->Load(array("alias" => $alias));

		self::view($customView,
			array(
				"pageTitle" => $pageTitle ?: $model->title,
				"metaKeywords" => $model->metaKeywords,
				"metaDescription" => $model->metaDescription,
				"model" => $model
			));
	}
}