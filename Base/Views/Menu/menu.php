<?php
/* @var \Modules\Base\Models\Menu[] $menu */

foreach ($menu as $menuItem):
	$cls = isset($class) ? array($class) : array();

	if ($menuItem->selected(isset($currentUrl) ? $currentUrl : null))
		$cls[] = "active";
?>
	<li <?php if (!empty($cls)): ?>class="<?=implode(' ', $cls)?>"<?php endif; ?>><a href="<?=$menuItem->url?>"><?=$menuItem->title?></a></li>
<?php endforeach; ?>