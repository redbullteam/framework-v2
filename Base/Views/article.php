<?php
/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var \Modules\Framework\Mvc\Model\BaseModel $model
 */
$this->extend('~:Layouts/general');
?>
<?php $__section->begin('body'); ?>
	<h1><?=$model->title?></h1>
	<?=$model->text?>
<?php $__section->end(); ?>