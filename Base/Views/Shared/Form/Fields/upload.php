<input type="hidden" id="<?=$field->fieldName?>" name="<?=$field->fieldName?>" value="<?=htmlspecialchars($model->{$field->fieldName} ?: $_REQUEST[$field->fieldName])?>">
<input type="file" class="input-xlarge" id="<?=$field->fieldName?>" name="<?=$field->fieldName?>" value="<?=htmlspecialchars($model->{$field->fieldName} ?: $_REQUEST[$field->fieldName])?>">
<?php if ($model->{$field->fieldName} && in_array(pathinfo($model->{$field->fieldName}, PATHINFO_EXTENSION), array("png", "gif", "jpg"))): ?>
	<ul class="help-block thumbnails">
		<li class="thumbnail">
			<a href="/upload/<?=$model->{$field->fieldName}?>"><img src="/upload/260x180/crop/<?=$model->{$field->fieldName}?>" alt="Просмотреть" style="display: block; max-width: 260px; max-height: 180px;"></a>
		</li>
	</ul>
<?php endif; ?>