<textarea class="input-xlarge wysiwyg" id="<?=$field->fieldName?>" name="<?=$field->fieldName?>"><?=htmlspecialchars($model->{$field->fieldName} ?: (isset($_REQUEST[$field->fieldName]) ? $_REQUEST[$field->fieldName] : ""))?></textarea>
<?php $__section->append('javascript'); ?>
	<script src="/Resources/Admin/js/FCKeditor/fckeditor.js"></script>
	<script>
		$(function () {
			$('.wysiwyg').each(function () {
				var oFCKeditor = new FCKeditor(this.name);
				oFCKeditor.BasePath	= '/Resources/Admin/js/FCKeditor/';
				oFCKeditor.ReplaceTextarea();
			});
		});
	</script>
<?php $__section->end(); ?>