<table id="<?=$field->fieldName?>" class="table table-striped">
	<?php $columns = $field->uiHint->enum ?: array("value"); ?>
	<?php $modelValue = $model->{$field->fieldName} ?: (isset($_REQUEST[$field->fieldName]) ? $_REQUEST[$field->fieldName] : array()); ?>
	<?php $length = count($modelValue); ?>
	<thead>
		<tr>
			<?php foreach ($columns as $column):?>
				<th><?=preg_replace('/([a-z0-9])([A-Z])/', "$1 $2", $column)?></th>
			<?php endforeach; ?>
		</tr>
	</thead>
	<tbody>
		<?php for ($i = 0; $i < $length; $i++):?>
			<tr>
				<?php foreach ($columns as $column):?>
					<td><input type="text" name="<?=$field->fieldName?>[<?=$i?>][<?=$column?>]" value="<?=$modelValue[$i]->{$column}?>"/></td>
				<?php endforeach; ?>
			</tr>
		<?php endfor; ?>
		<?php for ($i = $length; $i < $length + 4; $i++):?>
			<tr>
				<?php foreach ($columns as $column):?>
					<td><input type="text" name="<?=$field->fieldName?>[<?=$i?>][<?=$column?>]"/></td>
				<?php endforeach; ?>
			</tr>
		<?php endfor; ?>
	</tbody>
</table>