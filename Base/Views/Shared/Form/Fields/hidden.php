<?php $value = $model->{$field->fieldName} ?: (($field->validation->datatype == "boolean" || $field->validation->datatype == "integer" || $field->validation->datatype == "number")
                ? 0
                : "") ?>
<input type="hidden" id="<?=$field->fieldName?>" name="<?=$field->fieldName?>" value="<?=$value?>">