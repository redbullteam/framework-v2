<select class="input-xlarge" id="<?=$field->fieldName?>" name="<?=$field->fieldName?><?php if ($field->uiHint == "multiple"): ?>[]<?php endif; ?>" <?php if (!$this->disableRequired && $field->validation && $field->validation->required): ?>required<?php endif; ?> <?php if ($field->uiHint == "multiple"): ?>multiple style="height: 120px"<?php endif; ?>>
	<?php if ($field->uiHint != "multiple"): ?>
    <option></option>
	<?php endif; ?>
	<?php $options = $field->uiHint->sql
					? \Modules\Framework\Core\DB::Command()->query(sprintf($field->uiHint->sql, \Modules\Framework\Core\Config\Config::getInstance()->connection['dbPrefix'] . $model->getName()), PDO::FETCH_OBJ)
					: ($field->uiHint->enum ?: array("----")); ?>
	<?php $modelValue = $model->{$field->fieldName} ?: (isset($_REQUEST[$field->fieldName]) ? $_REQUEST[$field->fieldName] : null); ?>
	<?php foreach ($options as $option): ?>
		<?php $value = isset($option->value) ? $option->value : $option; ?>
		<option value="<?=$value?>" <?php if ((is_array($modelValue) && in_array($value, $modelValue)) || $modelValue == $value): ?>selected<?php endif?>><?=(isset($option->display) ? $option->display : $option)?></option>
	<?php endforeach; ?>
</select>