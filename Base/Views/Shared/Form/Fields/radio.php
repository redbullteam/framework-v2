<?php $options = $field->uiHint->sql
		? \Modules\Framework\Core\DB::Command()->query(sprintf($field->uiHint->sql, \Modules\Framework\Core\Config\Config::getInstance()->connection['dbPrefix'] . call_user_func($model->getName)), PDO::FETCH_OBJ)
		: ($field->uiHint->enum ?: array("----")); ?>
<?php foreach ($options as $option): ?>
	<?php $value = isset($option->value) ? $option->value : $option; ?>
	<?php $modelValue = $model->{$field->fieldName} ?: (isset($_REQUEST[$field->fieldName]) ? $_REQUEST[$field->fieldName] : null); ?>
	<label class="radio inline"><input type="radio" name="<?=$field->fieldName?>" value="<?=$value?>" <?php if (!$this->disableRequired && $field->validation && $field->validation->required): ?>required<?php endif; ?> <?php if ((is_array($modelValue) && in_array($value, $modelValue)) || $modelValue == $value): ?>checked<?phpendif?>><?=(isset($option->display) ? $option->display : $option)?></label>
<?php endforeach; ?>