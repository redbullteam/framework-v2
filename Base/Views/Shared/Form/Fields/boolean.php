<label class="checkbox">
    <input type="hidden" name="<?=$field->fieldName?>" value="0">
    <input type="checkbox" class="input-xlarge" id="<?=$field->fieldName?>" name="<?=$field->fieldName?>" value="1" <?php if (!$this->disableRequired && $field->validation && $field->validation->required): ?>required<?php endif; ?> <?php if ($model->{$field->fieldName}): ?>checked<?php endif; ?>>
	<?=$field->display?>
</label>