<input type="hidden" id="<?=$field->fieldName?>" name="<?=$field->fieldName?>" value="<?=htmlspecialchars($model->{$field->fieldName} ?: $_REQUEST[$field->fieldName])?>">
<div class="input-append filebrowser">
	<input type="text" class="span2" id="<?=$field->fieldName?>" name="<?=$field->fieldName?>" value="<?=htmlspecialchars($model->{$field->fieldName} ?: $_REQUEST[$field->fieldName])?>"><span class="add-on" style="cursor: pointer"><i class="icon-picture"></i></span>
</div>
<?php $__section->append('javascript'); ?>
	<script>
		var opener = null;

		function SetUrl(url) {
			$(opener).siblings('input').val(url);
			opener = null;
		}

		$(function () {
			$('.filebrowser .add-on').click(function () {
				var width = screen.width * 0.75,
					height = screen.height * 0.75,
					left = (screen.width - width) / 2,
					top = (screen.height = height) / 2;

				var options = "toolbar=no,status=no,resizable=yes,dependent=yes,scrollbars=yes" ;
				options += ",width=" + width;
				options += ",height=" + height;
				options += ",left=" + left;
				options += ",top=" + top;

				window.open('/Resources/Admin/js/FCKeditor/editor/filemanager/browser/default/browser.html?Connector=%2FResources%2FAdmin%2F%2Fjs%2FFCKeditor%2Feditor%2Ffilemanager%2Fconnectors%2Fphp%2Fconnector.php', 'FCKBrowseWindow', options);

				opener = this;
			});
		});
	</script>
<?php $__section->end(); ?>