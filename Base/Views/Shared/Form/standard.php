<?php
/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var \Modules\Framework\Mvc\Model\BaseModel $model
 */
?>
<form action="<?=isset($url) ? $url : ''?>" class="<?=isset($class) ? $class : 'form-horizontal'?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>
			<?=$pageTitle?>
        </legend>

		<?=$this->partial("formFields", array("model" => $model))?>

        <div class="<?=isset($formActionsClass) ? $formActionsClass : 'form-actions'?>">
            <button type="submit" class="btn btn-primary"><?=\Modules\Framework\Globalization\i18n::current()->send?></button>
        </div>
    </fieldset>
</form>