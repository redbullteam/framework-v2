<?php
/**
 * @var \Modules\Framework\Mvc\View\PhpView $this
 * @var \Modules\Framework\Mvc\View\Helpers\Section $__section
 * @var \Modules\Framework\Core\Metadata\DynamicMetadata $fields
 * @var \Modules\Framework\Mvc\Model\BaseModel $model
 */
?>
<?php foreach ($model->getModelMetadata() as $fieldName => $field): ?>
	<?php if ($field->hideFromDetails || ($field->backend && !$this->backend)) continue; ?>
	<?php if ($field->validation && $field->validation->datatype && $field->validation->datatype->dataType === "hidden" || ($field->debug && !$debugEnabled)): ?>
        <?=$this->partial("Fields/hidden", array('field' => $field, 'model' => $model))?>
		<?php continue; ?>
	<?php endif; ?>
	<div class="control-group <?php if ($field->validation && $field->validation->errorMessage): ?>error<?php endif; ?>">
		<?php if (!$field->validation || ($field->validation->datatype !== "boolean" && $field->validation->datatype !== "radio")):?>
			<label class="control-label" for="<?=$fieldName?>"><?=$field->display?><?php if ($field->validation && $field->validation->required): ?>*<?php endif; ?></label>
		<?php endif; ?>

		<div class="controls">
			<?=$this->partial("Fields/" . ($field->uiHint ?: ($field->validation && $field->validation->datatype ? $field->validation->datatype : "text")), array('field' => $field, 'model' => $model))?>

			<?php if ($field->validation && $field->validation->errorMessage): ?>
				<span class="help-inline"><?=$field->validation->errorMessage?></span>
			<?php endif; ?>
		</div>
	</div>
<?php endforeach; ?>