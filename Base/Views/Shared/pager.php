<?php
use Modules\Framework\Globalization\i18n;
/**
 * @var \Modules\Base\Models\PagerViewModel $pager
 * @var int $page
 */
?><?php if ($pager != null && $pager->totalPages > 1): ?>
	<nav class="pagination">
		<ul>
			<li class="pagination-prev<?=$pager->selected <= 1 ? ' disabled' : ''?>">
				<a href="<?=$pager->link(max($pager->selected - 1, 1))?>" title="<?=i18n::current()->previous?>">«</a>
			</li>

			<?php if ($pager->first > 1): ?>
			<li class="disabled"><a href="#">...</a></li>
			<?php endif; ?>

			<?php foreach ($pager->getPages() as $page): ?>
			<li<?=$page == $pager->selected ? ' class="active"' : ''?>>
				<a href="<?=$pager->link($page)?>"><?=$page?></a>
			</li>
			<?php endforeach; ?>

			<?php if ($pager->last < $pager->totalPages): ?>
			<li class="disabled"><a href="#">...</a></li>
			<?php endif; ?>

			<li class="pagination-next<?=$pager->selected >= $pager->totalPages ? ' disabled' : ''?>">
				<a href="<?=$pager->link(min($pager->selected + 1, $pager->totalPages))?>" title="<?=i18n::current()->next?>">»</a>
			</li>
		</ul>
	</nav>
<?php endif; ?>